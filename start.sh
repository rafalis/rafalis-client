#!/bin/bash

# edit configuration

ASSETS_CONFIGURATIONFILE=/srv/app/assets/configuration.json
ORIGINAL_CONFIGURATIONFILE=/srv/app/assets/configuration.json.original

if ! [ -a $ORIGINAL_CONFIGURATIONFILE ] ; then
  cp $ASSETS_CONFIGURATIONFILE $ORIGINAL_CONFIGURATIONFILE
fi

LAUNCH_DATE=$(date +%d/%m/%y)

echo "{
  \"rafalisServerURL\": \"$RAFALIS_SERVER_URL\",
  \"releaseVersion\": \"$RAFALIS_RELEASE_VERSION\",
  \"releaseDate\": \"$LAUNCH_DATE\"
}" \
  > $ASSETS_CONFIGURATIONFILE

# start nginx

exec nginx -g 'daemon off;'
