# npx ng build --configuration=production

FROM nginx:alpine

RUN apk add --no-cache bash jq

COPY dist/picportos-client /srv/app

COPY nginx-default.conf /etc/nginx/conf.d/default.conf

COPY start.sh /opt/

RUN chmod +x /opt/start.sh

CMD ["/opt/start.sh"]
