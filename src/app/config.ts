export interface Configuration {
  rafalisServerURL: string;
  releaseVersion: string;
  releaseDate: string;
}
