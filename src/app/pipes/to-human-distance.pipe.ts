import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'toHumanDistance'
})
export class ToHumanDistancePipe implements PipeTransform {

  transform(value: number, ...args: unknown[]): string {
    if (value < 1000) {
      return value + 'm';
    } else {
      return (value / 1000).toFixed(1) + 'km';
    }
  }
}
