import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'displayListProperly'
})
export class DisplayListProperlyPipe implements PipeTransform {

  transform(value: string, ...args: unknown[]): string {
    return value.split(',').join(', ');
  }

}
