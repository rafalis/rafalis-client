import {TestBed} from '@angular/core/testing';

import {RafalisWsService} from './rafalis-ws.service';

describe('PicportosWsService', () => {
  let service: RafalisWsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RafalisWsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
