import {TestBed} from '@angular/core/testing';

import {AuthWsService} from './auth-ws.service';

describe('AuthService', () => {
  let service: AuthWsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AuthWsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
