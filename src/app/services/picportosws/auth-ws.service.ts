import {Injectable} from '@angular/core';
import {RafalisWsService} from './rafalis-ws.service';
import {AuthRequest, AuthResponse} from '../../common/models/rafalis-model';

@Injectable({
  providedIn: 'root'
})
export class AuthWsService {

  private readonly ENDPOINT: string = 'auth';

  constructor(private picportosWsService: RafalisWsService) {
  }

  login(request: AuthRequest): Promise<AuthResponse> {
    return this.picportosWsService.post<AuthResponse>(this.ENDPOINT, request);
  }

}
