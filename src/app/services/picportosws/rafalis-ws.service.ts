import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {ConfigService} from '../config.service';

@Injectable({
  providedIn: 'root'
})
export class RafalisWsService {

  constructor(private httpClient: HttpClient,
              private configService: ConfigService) { }

  url(path: string): string {
    return `${this.configService.configuration.rafalisServerURL}/${path}`;
  }

  get<T>(path: string, params: any): Promise<T> {
    return this.httpClient.get<T>(this.url(path), {params}).toPromise().catch((error) => {
      throw this.transformError(error);
    });
  }

  post<T>(path: string, body: any): Promise<T> {
    return this.httpClient.post<T>(this.url(path),
      body, {}).toPromise().catch((error) => {
      throw this.transformError(error);
    });
  }

  postImage(path: string, body: any): Promise<any> {
    return this.httpClient.post<any>(this.url(path),
      body, {headers: {'Content-Type': 'multiform/form-data'}}).toPromise().catch((error) => {
      throw this.transformError(error);
    });
  }

  put<T>(path: string, body: any): Promise<T> {
    return this.httpClient.put<T>(this.url(path), body, {}).toPromise().catch((error) => {
      throw this.transformError(error);
    });
  }

  delete<T>(path: string): Promise<T> {
    return this.httpClient.delete<T>(this.url(path), {}).toPromise().catch((error) => {
      throw this.transformError(error);
    });
  }


  getBlob(path: string, params: any): Promise<HttpResponse<Blob>> {
    return this.httpClient.get(this.url(path), {params, observe: 'response', responseType: 'blob'}).toPromise().catch((error) => {
      throw this.transformError(error);
    });
  }

  transformError(error: any): RafalisWsError {
    try {
      return new RafalisWsError(error.status, error.error.message);
    } catch (e) {
      return new RafalisWsError(error.status, error.message);
    }
  }
}

export class RafalisWsError {
  constructor(public code: number,
              public message: string) {
  }
}
