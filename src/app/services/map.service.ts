import {EventEmitter, Injectable, SecurityContext} from '@angular/core';
import {RafalisWsService} from './picportosws/rafalis-ws.service';
import {IMAGE_QUALITY, MapAddedConfirmation, MapSummary, MapUpdateStatus} from '../common/models/rafalis-model';
import {HttpResponse} from '@angular/common/http';
import {ImageService} from './image.service';
import {DomSanitizer} from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class MapService {
  private readonly MAP_ENDPOINT = 'map';
  static readonly RESERVED_NAME: string = 'Rafalis map';
  static readonly RESERVED_FILENAME = 'rafalis_system_map';

  closed = new EventEmitter<void>();

  constructor(private readonly ws: RafalisWsService,
              private readonly imageService: ImageService,
              private sanitizer: DomSanitizer) {
  }

  insertMap(file: FormData): Promise<MapAddedConfirmation> {
    return this.ws.post<MapAddedConfirmation>(`${this.MAP_ENDPOINT}`, file);
  }

  getMapSummaries(): Promise<MapSummary[]> {
    return this.ws.get<MapSummary[]>(`${this.MAP_ENDPOINT}/sum`, {});
  }

  getMapSummary(id: number): Promise<MapSummary> {
    return this.ws.get<MapSummary>(`${this.MAP_ENDPOINT}/${id}`, {});
  }

  getGeoJson(id: number): Promise<HttpResponse<Blob>> {
    return this.ws.getBlob(`${this.MAP_ENDPOINT}/${id}/geojson`, {});
  }

  updateMap(newMap: FormData, id: number): Promise<MapUpdateStatus> {
    return this.ws.put<MapUpdateStatus>(`${this.MAP_ENDPOINT}/${id}`, newMap);
  }

  deleteMap(id: number): Promise<void> {
    return this.ws.delete<void>(`${this.MAP_ENDPOINT}/${id}`);
  }

  parseDescription(description: string): ParsedDescription {
    let imageID: number;
    let additionnalParagraphs = [];
    for (const descLine of description.split('<br>')) {
      if (descLine === '') {
        continue;
      }
      if (!descLine.includes('!img')) {
        additionnalParagraphs.push(descLine);
      } else {
        imageID = +descLine.replace(/^\D+/g, '');
      }
    }
    return {imageID, additionnalParagraphs};
  }

  async generateDescription(initialDesc: string, initialName: string = ''): Promise<string> {
    let desc: string;

    initialDesc = initialDesc.replace('\n', '<br>');

    const parsed = this.parseDescription(initialDesc);

    const description = await this.imageService.getImageDescription(parsed.imageID);
    const blobResponse = await this.imageService.getImageBytes(parsed.imageID, IMAGE_QUALITY.THUMB_QUALITY);
    const imageURL = this.sanitizer.sanitize(SecurityContext.URL,
      this.imageService.createImageFromBlob(blobResponse.body));

    let imageInfos = '';
    const separator = ' | ';
    if (description.datetime && description.datetime !== '-') {
      imageInfos += description.datetime;
    }
    if (description.author && description.author !== '-') {
      if (imageInfos) {
        imageInfos += separator;
      }
      imageInfos += description.author.split(',').join(', ');
    }
    if (description.camera && description.camera !== '-') {
      if (imageInfos) {
        imageInfos += separator;
      }
      imageInfos += description.camera;
    }

    desc =
      `<h6 title="${description.title}">${description.title}</h6>
      <hr>
      <div matRipple class="map-image-wrapper" onclick="window.open('https://' + location.host + '/img?id=${parsed.imageID}', '_blank')">
          <img src="${imageURL}"
               title="Open Image"
               alt="preview"
               class="preview-image-popup picportos-image-h">
      </div>
      <p>${imageInfos}</p>`;
    if (parsed.additionnalParagraphs.length > 0) {
      desc += `<p>${parsed.additionnalParagraphs.join('</p><p>')}</p>`;
    }
    return desc;
  }

  orderMaps(maps: MapSummary[]): MapSummary[] {
    maps = maps.sort((a, b) => a.name.localeCompare(b.name));
    const systemMapIndex = maps.findIndex((m) => m.filename === MapService.RESERVED_FILENAME);
    // put reserved map up front
    const tmp = maps[0];
    maps[0] = maps[systemMapIndex];
    maps[systemMapIndex] = tmp;
    return maps;
  }

  printMapName(name: string): string {
    if (name === MapService.RESERVED_NAME) {
      return '[Base] ' + name;
    }
    return name;
  }

  generateEmptyGeoJson(name: string): string {
    return JSON.stringify({
      'name': name,
      'type': 'FeatureCollection',
      'features': []
    });
  }
}

export interface ParsedDescription {
  additionnalParagraphs: string[];
  imageID: number;
}
