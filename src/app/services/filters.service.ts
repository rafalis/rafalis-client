import {EventEmitter, Injectable} from '@angular/core';
import {SORT_BY} from '../common/filters/filters.component';
import {ImageItem} from '../common/models/rafalis-model';


export interface Filters {
  keywords: string;
  publicOnly: boolean;
  color: string;
}

export interface SortBy {
  recent: boolean;
  title: boolean;
  ascending: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class FiltersService {

  readonly DEFAULT_SORTCONFIG = {recent: true, title: false, ascending: false};
  readonly DEFAULT_SORTSTR = SORT_BY.RECENT_DESC;

  private allAvailableTags = new Set<string>();
  private publicAvailableTags = new Set<string>();
  filters: Filters = {keywords: '', publicOnly: false, color: ''};
  sortbyConfig: SortBy = this.DEFAULT_SORTCONFIG;
  sortbyStr: SORT_BY = this.DEFAULT_SORTSTR;
  publicToggleEmitter = new EventEmitter<boolean>();
  newSortByEmitter = new EventEmitter<SORT_BY>();
  newTagsAvailableEmitter = new EventEmitter<string[]>();

  constructor() {
  }

  updateAvailableTags(items: ImageItem[]): void {
    this.allAvailableTags.clear();
    this.publicAvailableTags.clear();
    items.forEach(item => {
      const tags = item.tags.toLowerCase()
        .split(',').filter(tag => tag !== '');
      tags.forEach(tag => this.allAvailableTags.add(tag));
      if (item.ispublic) {
        tags.forEach(tag => this.publicAvailableTags.add(tag));
      }
    });

    this.newTagsAvailableEmitter.emit(this.filters.publicOnly ? [...this.publicAvailableTags].sort() :
      [...this.allAvailableTags].sort());
  }

  onSortByChange(sortby: SORT_BY): void {
    this.sortbyStr = sortby;
    this.newSortByEmitter.emit(this.sortbyStr);
  }
}
