import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MobileService {

  readonly MOBILE_WIDTH_THRESHOLD = 1024;  // px
  isMobileScreen = false;

  constructor() {
    this.isMobileScreen = innerWidth < this.MOBILE_WIDTH_THRESHOLD;
  }

}
