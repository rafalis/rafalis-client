import {Injectable} from '@angular/core';
import {RafalisWsService} from './picportosws/rafalis-ws.service';
import {AdminConfig, AdminConfigUpdateStatus} from '../common/models/rafalis-model';
import {MatSnackBar} from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class AppConfigService {
  private readonly USER_ENDPOINT = 'appconfig';

  private adminConfig: AdminConfig;
  private configOK = false;

  constructor(private readonly ws: RafalisWsService,
              private snackbar: MatSnackBar) {
    this.retrieveConfig();
    this.ensureConfigIsRetrieved();
  }

  ensureConfigIsRetrieved(): void {
    if (!this.configOK) {
      setTimeout(() => {
        this.retrieveConfig();
        this.ensureConfigIsRetrieved();
      }, 10000);
    }
  }

  invalidateConfig(): void {
    this.configOK = false;
    this.retrieveConfig();
  }

  private retrieveConfig(): void {
    if (this.configOK) {
      return;
    }
    this.getConfig().then(res => {
      this.adminConfig = res;
      this.configOK = true;
    }, err => {
      this.snackbar.open('Failed to connect to server. Retrying in 10 sec...', '', {duration: 10000});
      return err;
    });
  }

  get userAuthors(): string[] {
    return this.adminConfig?.authors;
  }

  get userCameras(): string[] {
    return this.adminConfig?.cameras;
  }

  get userMainTitle(): string {
    return this.adminConfig?.mainTitle;
  }

  get userSubTitle(): string {
    return this.adminConfig?.subTitle;
  }

  async getConfig(): Promise<AdminConfig> {
    return this.ws.get<AdminConfig>(`${this.USER_ENDPOINT}`, {});
  }

  async modifyConfig(newConfig: AdminConfig): Promise<AdminConfigUpdateStatus> {
    return this.ws.put(`${this.USER_ENDPOINT}`, newConfig);
  }
}
