import {Injectable} from '@angular/core';
import {RafalisWsService} from './picportosws/rafalis-ws.service';
import {Collection, CollectionSummary, CollectionUpdateStatus} from '../common/models/rafalis-model';

@Injectable({
  providedIn: 'root'
})
export class CollectionService {

  private readonly COLLECTION_ENDPOINT = 'collection';

  constructor(private ws: RafalisWsService) { }

  getCollection(id: number): Promise<Collection> {
    return this.ws.get(`${this.COLLECTION_ENDPOINT}/${id}`, {});
  }

  getCollectionSummary(): Promise<CollectionSummary[]> {
    return this.ws.get(`${this.COLLECTION_ENDPOINT}`, {});
  }

  modifyCollection(newCollection: Collection): Promise<CollectionUpdateStatus> {
    return this.ws.put(`${this.COLLECTION_ENDPOINT}/${newCollection.id}`, newCollection);
  }

  postCollection(newCollection: Collection): Promise<CollectionUpdateStatus> {
    return this.ws.post(`${this.COLLECTION_ENDPOINT}`, newCollection);
  }

  deleteCollection(id: number): Promise<void> {
    return this.ws.delete(`${this.COLLECTION_ENDPOINT}/${id}`);
  }
}
