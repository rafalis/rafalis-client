import {Injectable} from '@angular/core';
import {JwtHelperService} from '@auth0/angular-jwt';
import {AuthWsService} from './picportosws/auth-ws.service';
import {Router} from '@angular/router';
import {MatSnackBar} from '@angular/material/snack-bar';
import {RafalisToken} from '../common/models/rafalis-model';

export enum PicportosRank {
  ADMIN = 'creator',
  VIP = 'viewer'
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private decodedToken: RafalisToken;
  private helper = new JwtHelperService();

  constructor(private snackbar: MatSnackBar,
              private authWsService: AuthWsService,
              private router: Router) {
    const token = AuthService.localToken;
    this.decodeToken(token);
  }

  private decodeToken(token: string): void {
    this.decodedToken = token === null ? null : this.helper.decodeToken(token) as RafalisToken;
  }

  async login(username: string, password: string): Promise<void> {
    const response = await this.authWsService.login({username, password});
    localStorage.setItem('picportosUserToken', response.token);
    this.decodeToken(response.token);
    this.snackbar.open(`Welcome ${this.picportosToken.email} !`, '', {duration: 3000});
  }

  logout(): void {
    localStorage.removeItem('picportosUserToken');
    this.router.navigateByUrl('/');
    this.decodedToken = null;
    this.snackbar.open('You have been logged out.', '', {duration: 3000});
  }

  get authToken(): string {
    return AuthService.localToken;
  }

  get picportosToken(): RafalisToken {
    this.decodeToken(AuthService.localToken);
    return this.decodedToken;
  }

  private static get localToken(): string {
    return localStorage.getItem('picportosUserToken');
  }

  get authenticated(): boolean {
    const token = AuthService.localToken;
    if (token === null) {
      return false;
    }
    if (this.helper.getTokenExpirationDate(token).valueOf() < new Date().valueOf()) {
      this.logout();
      return false;
    }
    return true;
  }

  get rank(): PicportosRank {
    const token = AuthService.localToken;
    this.decodeToken(token);
    if (this.decodedToken.admin) {
      return PicportosRank.ADMIN;
    } else {
      return PicportosRank.VIP;
    }
  }

  get isAdmin(): boolean {
    return this.decodedToken?.admin;
  }

  loginBeforeAccessingContent(): void {
    this.snackbar.open('Accessing private content requires authentication.', '', {duration: 5000, verticalPosition: "top"});
    this.router.navigate(['login'], this.router.url === '/' ? {} : {queryParams: {returnUrl: this.router.url}});
  }

}
