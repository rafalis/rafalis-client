import {EventEmitter, Injectable} from '@angular/core';
import {Filters, FiltersService} from './filters.service';
import {DomSanitizer, SafeUrl} from '@angular/platform-browser';
import {
  IMAGE_QUALITY,
  ImageAddedConfirmation,
  ImageDescription,
  ImageDescriptionUpdate,
  ImageDescriptionUpdatedStatus,
  ImageItem
} from '../common/models/rafalis-model';
import {RafalisWsService} from './picportosws/rafalis-ws.service';
import {HttpResponse} from '@angular/common/http';
import {ColorService} from './color.service';
import {AuthService} from "./auth.service";

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  idsToDisplay: number[];
  idsToDisplayNoHidden: number[];
  currentItems: ImageItem[] = [];
  emitter: EventEmitter<ImageItem[]> = new EventEmitter<ImageItem[]>();
  private readonly IMAGE_ENDPOINT = 'img';
  // any minithumb larger than this value is considered a panorama
  readonly PANORAMA_THRESHOLD_MINITHUMB_PX = 290;

  constructor(private readonly filtersService: FiltersService,
              private dom: DomSanitizer,
              private ws: RafalisWsService,
              private authService: AuthService,
              private colorService: ColorService) {
    this.idsToDisplay = [];
    this.idsToDisplayNoHidden = [];
  }

  thumbNailUrlFromId(id: number): string {
    return `${this.ws.url(this.IMAGE_ENDPOINT)}/${id}?q=${IMAGE_QUALITY.THUMB_QUALITY}`;
  }

  postImage(file: FormData): Promise<ImageAddedConfirmation> {
    return this.ws.post<ImageAddedConfirmation>(`${this.IMAGE_ENDPOINT}`, file);
  }

  createImageFromBlob(data: Blob): SafeUrl {
    return this.dom.bypassSecurityTrustUrl(URL.createObjectURL(data));
  }

  async updateImageItems(): Promise<void> {
    const newItems = await this.getList(this.filtersService.filters);
    if (this.filtersService.filters.color !== '') {
      this.currentItems = newItems.filter((imageItem: ImageItem) =>
        this.colorService.isPaletteCloseToColor(imageItem.palette, this.filtersService.filters.color));
    } else {
      this.currentItems = newItems;
    }
    this.idsToDisplay = [];
    this.idsToDisplayNoHidden = [];
    this.currentItems.forEach(value => {
      this.idsToDisplay.push(value.id);
      if (value.show_in_browse) {
        this.idsToDisplayNoHidden.push(value.id);
      }
    });
    this.emitter.emit(this.currentItems);
  }

  updateImageItemsOrder(items: ImageItem[]): void {
    this.idsToDisplayNoHidden = [];
    items.forEach(i => this.idsToDisplayNoHidden.push(i.id));
  }

  getImageBytes(id: number, quality: IMAGE_QUALITY = IMAGE_QUALITY.SMALL_QUALITY): Promise<HttpResponse<Blob>> {
    return this.ws.getBlob(`${this.IMAGE_ENDPOINT}/${id}?q=${quality}`, {responseType: 'image/jpeg'});
  }

  getImageURL(id: number, quality: IMAGE_QUALITY = IMAGE_QUALITY.SMALL_QUALITY): string {
    return this.ws.url(`img/${id}?q=${quality}`);
  }

  getImageDescription(id: number): Promise<ImageDescription> {
    return this.ws.get(`${this.IMAGE_ENDPOINT}/desc/${id}`, {});
  }

  getList(keywords: Filters): Promise<ImageItem[]> {
    const kw = keywords.keywords.replace(' ', ',')
      .replace(', ', ',');
    const color = keywords.color;
    return this.ws.get(`${this.IMAGE_ENDPOINT}?keywords=${kw}&color=${color}`, {});
  }

  updateDescription(id: number, newDescription: ImageDescriptionUpdate): Promise<ImageDescriptionUpdatedStatus> {
    return this.ws.put<ImageDescriptionUpdatedStatus>(`${this.IMAGE_ENDPOINT}/desc/${id}`, newDescription);
  }

  deleteImage(id: number): Promise<void> {
    return this.ws.delete<void>(`${this.IMAGE_ENDPOINT}/${id}`);
  }

  getRandomDescription(): Promise<ImageDescription> {
    return this.ws.get('img/random', {});
  }

  rotateImage(id: number, rotation: number, qualities: number[]): Promise<ImageDescriptionUpdatedStatus> {
    const q = qualities.join('&q=');
    return this.ws.post(`${this.IMAGE_ENDPOINT}/${id}/rotate?rot=${rotation}&q=${q}`, {});
  }

  getOverlayImageTitle(imageItem: ImageDescription | ImageItem): string {
    let desc = '';
    if (this.authService.authenticated) {
      desc += `${imageItem.ispublic ? '[Public] ' : '[Private] '}`;
    }
    if (imageItem.datetime) {
      desc += `#${imageItem.id} | ${imageItem.datetime} | ${imageItem.title}`;
    } else {
      desc += `#${imageItem.id} | ${imageItem.title}`;
    }
    return desc;
  }
}
