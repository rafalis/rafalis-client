import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Attributes, IntersectionObserverHooks} from 'ng-lazyload-image';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable()
export class LazyLoadImageHooks extends IntersectionObserverHooks {
  private http: HttpClient;

  constructor(http: HttpClient) {
    super();
    this.http = http;
  }

  loadImage({imagePath}: Attributes): Observable<string> {
    // Intercept http requests from the lazyload to add them the auth token
    return this.http.get(imagePath, {responseType: 'blob'}).pipe(map(blob => URL.createObjectURL(blob)));
  }
}
