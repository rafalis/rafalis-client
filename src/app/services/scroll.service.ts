import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ScrollService {

  browseScrollY: number = 0;
  scrollBackEnabled = false;

  constructor() {
  }

  enableScrollBack(): void {
    this.scrollBackEnabled = true;
  }

  disableScrollBack(): void {
    this.scrollBackEnabled = false;
    this.browseScrollY = 0;
  }
}
