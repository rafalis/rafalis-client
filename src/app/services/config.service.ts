import {Injectable} from '@angular/core';
import {Configuration} from '../config';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  configuration: Configuration;

  constructor() { }

}
