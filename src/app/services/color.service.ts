import {Injectable} from '@angular/core';

export interface Color {
  r: number;
  g: number;
  b: number;
}

@Injectable({
  providedIn: 'root'
})
export class ColorService {

  private readonly LIGHTEN_FACTOR = 1.1;
  private readonly COLOR_DISTANCE_THRESHOLD = 90;

  constructor() {
  }

  rgbToHex(red: number, green: number, blue: number): string {
    const rgb = (red << 16) | (green << 8) | (blue << 0);
    return '#' + (0x1000000 + rgb).toString(16).slice(1);
  }

  hexToRgb(hex: string): number[] {
    const normal = hex.match(/^#([0-9a-f]{2})([0-9a-f]{2})([0-9a-f]{2})$/i);
    if (normal) {
      return normal.slice(1).map(e => parseInt(e, 16));
    }
    const shorthand = hex.match(/^#([0-9a-f])([0-9a-f])([0-9a-f])$/i);
    if (shorthand) {
      return shorthand.slice(1).map(e => 0x11 * parseInt(e, 16));
    }
    return null;
  }

  redistributeRGB(r_in: number, g_in: number, b_in: number, factor: number): number[] {
    let r = r_in * factor;
    let g = g_in * factor;
    let b = b_in * factor;

    const threshold = 254.999;
    const m = Math.max(r, g, b);
    if (m <= threshold) {
      if (this.isTooBlack(r, g, b)) {
        // don't apply any color then
        return [-1, -1, -1];
      }
      return [Math.round(r), Math.round(g), Math.round(b)];
    }
    const total = r + g + b;
    if (total >= 3 * threshold) {
      return [threshold, threshold, threshold];
    }
    const x = (3 * threshold - total) / (3 * m - total);
    const gray = threshold - x * m;
    return [Math.round(gray + x * r), Math.round(gray + x * g), Math.round(gray + x * b)];
  }

  lightenPalette(palette: string[]): string[] {
    return palette
      .map(hexString => this.hexToRgb(hexString))
      .map(rgbArray => this.redistributeRGB(rgbArray[0], rgbArray[1], rgbArray[2], this.LIGHTEN_FACTOR))
      .sort((c1, c2) =>
        c2.reduce((a, b) => a + b, 0) - c1.reduce((a, b) => a + b, 0))
      .map(rgbArray => this.rgbToHex(rgbArray[0], rgbArray[1], rgbArray[2]));
  }

  isTooBlack(r: number, g: number, b: number): boolean {
    const luma = 0.2126 * r + 0.7152 * g + 0.0722 * b; // per ITU-R BT.709
    return luma < 40;
  }

  getColourDistance(color1Hex: string, color2Hex: string): number {
    const color1 = this.hexToRgb(color1Hex);
    const color2 = this.hexToRgb(color2Hex);
    /* from https://www.compuphase.com/cmetric.htm */
    const rmean = color1[0] + (color2[0]) / 2;
    const r = color1[0] - color2[0];
    const g = color1[1] - color2[1];
    const b = color1[2] - color2[2];
    return Math.sqrt((((512 + rmean) * r * r) >> 8) + 4 * g * g + (((767 - rmean) * b * b) >> 8));
  }

  isPaletteCloseToColor(palette: string[], targetColor: string): boolean {
    const distances = palette.slice(0, 3)
      .map((palette) => this.getColourDistance(palette, targetColor));
    return distances.some((dist: number) => dist < this.COLOR_DISTANCE_THRESHOLD);
  }

}
