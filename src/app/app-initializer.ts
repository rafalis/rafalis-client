import {ConfigService} from './services/config.service';
import {HttpClient} from '@angular/common/http';
import {Configuration} from './config';

export const appInitializer = (
  configurationService: ConfigService,
  httpClient: HttpClient): () => Promise<any> => (): Promise<any> => httpClient.get('/assets/configuration.json')
  .toPromise()
  .then(response => {
    configurationService.configuration = response as Configuration;
  });
