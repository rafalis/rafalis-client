import {Component} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {ConfigService} from "../../services/config.service";
import {RafalisWsService} from "../../services/picportosws/rafalis-ws.service";
import {VersionInfo} from "../../common/models/rafalis-model";

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent {

  private readonly GITLAB_URL = 'https://gitlab.com/rafalis/rafalis';
  client_release_version: string;
  client_release_date: string;
  server_release_version: string;
  server_release_date: string;

  constructor(private titleService: Title,
              private configurationService: ConfigService,
              private rafalisWSService: RafalisWsService) {
    this.titleService.setTitle('Rafalis - About');
    this.client_release_version = configurationService.configuration.releaseVersion;
    this.client_release_date = configurationService.configuration.releaseDate;
    rafalisWSService.get<VersionInfo>('appconfig/version', {}).then((res) => {
      this.server_release_version = res.version;
      this.server_release_date = res.date;
    });
  }

  openGitLab(): void {
    window.open(this.GITLAB_URL);
  }
}
