import {Component, EventEmitter} from '@angular/core';
import {UntypedFormBuilder, UntypedFormGroup, Validators} from '@angular/forms';
import {CollectionService} from '../../services/collection.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {IMAGE_QUALITY, ImageItem} from '../../common/models/rafalis-model';
import {ImageService} from '../../services/image.service';

@Component({
  selector: 'app-add-collection',
  templateUrl: './add-collection.component.html',
  styleUrls: ['./add-collection.component.scss']
})
export class AddCollectionComponent {

  submitted = new EventEmitter();
  addCollectionForm: UntypedFormGroup;
  thumbnailSelector = false;
  selectedThumbnail: any;
  uploading = false;

  constructor(private formBuilder: UntypedFormBuilder,
              private readonly collectionService: CollectionService,
              private snackBar: MatSnackBar,
              private imageService: ImageService) {
    this.addCollectionForm = this.formBuilder.group({
      name: ['', Validators.required],
      description: ['', Validators.required],
      thumbnailid: ['', Validators.required],
      ispublic: ['']
    });
  }

  async onSubmit(): Promise<void> {
    if (!this.addCollectionForm.valid) {
      return;
    }
    this.uploading = true;
    const collection = {
      id: null,
      name: this.addCollectionForm.value.name,
      description: this.addCollectionForm.value.description,
      thumbnailid: this.addCollectionForm.value.thumbnailid,
      structure: '',
      textContent: [],
      imageContent: [],
      carouselContent: [],
      ispublic: this.addCollectionForm.value.ispublic,
      map_id: null
    };
    try {
      const status = await this.collectionService.postCollection(collection);
      if (status.message === 'success') {
        this.snackBar.open('Collection successfully created !', 'OK', {duration: 5000});
      }
      this.submitted.emit();
      this.uploading = false;
    } catch (e) {
      this.snackBar.open('Error while creating the collection.', '', {duration: 5000, verticalPosition: 'top'});
    }
    this.uploading = false;
  }

  enableThumbnailSelector(): void {
    this.thumbnailSelector = true;
  }

  async thumbnailSelected(imageItem: ImageItem[]): Promise<void> {
    this.addCollectionForm.patchValue({thumbnailid: imageItem[0].id});
    this.selectedThumbnail = this.imageService.getImageURL(imageItem[0].id, IMAGE_QUALITY.THUMB_QUALITY);
    this.thumbnailSelector = false;
  }

}
