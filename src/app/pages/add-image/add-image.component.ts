import {Component, EventEmitter, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {UntypedFormBuilder, UntypedFormGroup, Validators} from '@angular/forms';
import {ImageService} from '../../services/image.service';
import {ImageDescriptionUpdate} from '../../common/models/rafalis-model';
import {MatSnackBar} from '@angular/material/snack-bar';
import {DatePipe} from '@angular/common';
import {MatChipInputEvent} from '@angular/material/chips';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {Router} from '@angular/router';
import {AppConfigService} from '../../services/app-config.service';
import {MatSlideToggleChange} from '@angular/material/slide-toggle';

export interface Tag {
  name: string;
}

@Component({
  selector: 'app-add-image',
  templateUrl: './add-image.component.html',
  styleUrls: ['./add-image.component.scss']
})
export class AddImageComponent {

  static closed = new EventEmitter<void>();
  readonly separatorKeysCodes = [ENTER, COMMA] as const;
  file: File = null;
  multipleFiles: {file: File, uploaded: boolean, error: boolean}[] = [];
  newImageForm: UntypedFormGroup;
  uploading: boolean;
  dateUnknown: boolean;
  thumbnail: any;
  mobile = false;
  tags: Tag[] = [];
  isMultipleFiles: boolean = false;
  multipleFilesCounter = 0;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              private formBuilder: UntypedFormBuilder,
              private imageService: ImageService,
              private snackBar: MatSnackBar,
              private datepipe: DatePipe,
              private router: Router,
              readonly userService: AppConfigService) {
    this.newImageForm = this.formBuilder.group({
      description: [''],
      title: ['', Validators.required],
      author: ['', Validators.required],
      datetime: ['', Validators.required],
      camera: ['', Validators.required],
      tags: [''],
      ispublic: [''],
      hide_in_browse: [''],
      show_in_home: ['']
    });
    this.mobile = data?.mobile;
  }

  add(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();
    if (value) {
      this.tags.push({name: value});
      this.newImageForm.patchValue({tags: this.tags.map(v => v.name).join(',')});
    }
    event.input.value = '';
  }

  remove(tag: Tag): void {
    const index = this.tags.indexOf(tag);
    if (index >= 0) {
      this.tags.splice(index, 1);
      this.newImageForm.patchValue({tags: this.tags.map(v => v.name).join(',')});
    }
  }

  onClose(): void {
    AddImageComponent.closed.emit();
  }

  async onFileSelected(event): Promise<void> {
    if (this.file) {
      this.thumbnail = null;
    }
    if (this.multipleFiles) {
      this.multipleFiles = [];
    }
    if (event.target.files.length > 1) {
      if (event.target.files.length > 100) {
        this.snackBar.open("Upload a maximum of 100 files.", "", {duration: 5000, verticalPosition: 'top'});
        this.multipleFiles = [];
        return;
      }
      this.isMultipleFiles = true;
      for (const curFile of event.target.files) {
        this.multipleFiles.push({file: curFile, uploaded: false, error: false});
        if (!this.newImageForm.value.title.includes("#")) {
          this.newImageForm.patchValue({title: this.newImageForm.value.title === '' ? 'new_upload_#' : this.newImageForm.value.title + "_#"});
        }
      }
    } else {
      this.isMultipleFiles = false;
      this.file = event.target.files[0];
      const buffer = await this.file.arrayBuffer();
      this.thumbnail = this.imageService.createImageFromBlob(new Blob([buffer]));
      if (this.newImageForm.value.title === '') {
        this.newImageForm.patchValue({title: event.target.files[0].name});
      }
    }
  }

  private parseDate(date: Date): string {
    return this.datepipe.transform(date, 'dd-MM-YYYY');
  }

  async submitForm(): Promise<void> {
    if ((!this.file && !this.isMultipleFiles && this.multipleFiles.length === 0) || !this.newImageForm.valid) {
      return;
    }
    this.uploading = true;
    if (this.isMultipleFiles) {
      this.multipleFilesCounter = 0;
      const errors = [];
      for (let i = 0; i < this.multipleFiles.length; i++) {
        const formData = new FormData();
        const desc: ImageDescriptionUpdate = {
          tags: this.newImageForm.value.tags || '',
          description: this.newImageForm.value.description,
          title: this.newImageForm.value.title.replace("#", String(i).padStart(2, "0")),
          datetime: this.parseDate(this.newImageForm.value.datetime),
          camera: this.newImageForm.value.camera,
          author: this.newImageForm.value.author.join(','),
          ispublic: this.newImageForm.value.ispublic,
          show_in_browse: !this.newImageForm.value.hide_in_browse,
          show_in_home: this.newImageForm.value.show_in_home
        };
        formData.append('image', this.multipleFiles[i].file);
        formData.append('name', this.multipleFiles[i].file.name);
        formData.append('description', JSON.stringify(desc));
        let result;
        try {
          result = await this.imageService.postImage(formData);
          this.multipleFilesCounter++;
          this.multipleFiles[i].uploaded = true;
        } catch (e) {
          this.multipleFiles[i].error = true;
          errors.push("<"+this.multipleFiles[i].file.name + ">: " + (e.message || "Unknown error"));
        }
      }
      this.uploading = false;
      if (errors.length > 0) {
        this.snackBar.open(`Successfully added ${this.multipleFilesCounter}/${this.multipleFiles.length} images to the database.
        The following images could not be uploaded : ${errors.join("\n")}`, "Understood", {verticalPosition: "top"});
        return ;
      }
      this.snackBar.open(`Successfully added ${this.multipleFilesCounter}/${this.multipleFiles.length} images to the database.`, 'OK', {duration: 5000});
      this.onClose();
      this.multipleFiles = [];
    } else {
      const formData = new FormData();
      const desc: ImageDescriptionUpdate = {
        tags: this.newImageForm.value.tags || '',
        description: this.newImageForm.value.description,
        title: this.newImageForm.value.title,
        datetime: this.parseDate(this.newImageForm.value.datetime),
        camera: this.newImageForm.value.camera,
        author: this.newImageForm.value.author.join(','),
        ispublic: this.newImageForm.value.ispublic,
        show_in_browse: !this.newImageForm.value.hide_in_browse,
        show_in_home: this.newImageForm.value.show_in_home
      };
      formData.append('image', this.file);
      formData.append('name', this.file.name);
      formData.append('description', JSON.stringify(desc));
      let result;
      try {
        result = await this.imageService.postImage(formData);
      } catch (e) {
        if (e.code === 400 && e.message) {
          this.snackBar.open(e.message, 'OK', {duration: 5000, verticalPosition: 'top'});
        } else {
          this.snackBar.open('Failed to add the new image.', 'OK', {duration: 5000, verticalPosition: 'top'});
        }
        this.uploading = false;
        return;
      }
      this.uploading = false;
      if (result.message === 'success') {
        this.snackBar.open('Image successfully added !', 'Show me', {duration: 5000});
        this.snackBar._openedSnackBarRef.onAction().subscribe(() => this.router.navigateByUrl(`/img?id=${result.id}`));
        this.onClose();
      }
    }
  }

  toggleDate(): void {
    this.dateUnknown = !this.dateUnknown;
    this.dateUnknown ? this.newImageForm.get('datetime').disable() : this.newImageForm.get('datetime').enable();
  }

  onHomePageChange($event: MatSlideToggleChange): void {
    if ($event.checked && !this.newImageForm.value.ispublic) {
      this.newImageForm.patchValue({ispublic: true});
    }
  }
}
