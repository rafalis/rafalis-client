import {Component, ElementRef, HostListener, OnInit, ViewChild} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {ImageService} from '../../services/image.service';
import {Subscription} from 'rxjs';
import {AppConfigService} from '../../services/app-config.service';
import {AddImageComponent} from '../add-image/add-image.component';
import {MatDialog} from '@angular/material/dialog';
import {MobileService} from '../../services/mobile.service';
import {FullImageParams, ImageDescription} from '../../common/models/rafalis-model';
import {AddCollectionComponent} from '../add-collection/add-collection.component';
import {FullImageComponent} from "../image/full-image/full-image.component";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  @ViewChild('scrollablediv') scrollableDiv: ElementRef;
  @ViewChild('fullImage') imageOverlayChild: FullImageComponent;
  randomDescription: ImageDescription;
  cannotShowImage = false;
  subscription: Subscription;
  backgroundClass = 'home-image-zoom';
  mobile: boolean;

  @HostListener('contextmenu', ['$event'])
  onRightClick(event): void {
    event.preventDefault();
  }
  constructor(readonly authService: AuthService,
              readonly imageService: ImageService,
              readonly appConfigService: AppConfigService,
              private dialog: MatDialog,
              private mobileService: MobileService) {
    this.mobile = this.mobileService.isMobileScreen;
  }

  async ngOnInit(): Promise<void> {
    await this.getRandomImage();
  }

  async getRandomImage(): Promise<void> {
    this.backgroundClass = 'home-image-zoom';
    try {
      this.randomDescription = await this.imageService.getRandomDescription();
      this.cannotShowImage = false;
      setTimeout(() => {
        this.backgroundClass += ' home-image-zoom-in';
      }, 100);
    } catch (e) {
      if (e.code === 404) {
        console.log('Unable to find a random image eligible.');
        this.cannotShowImage = true;
      }
    }
  }

  openAddImageDialog(): void {
    this.dialog.open(AddImageComponent, {
      data: {mobile: this.mobileService.isMobileScreen},
      width: this.mobileService.isMobileScreen ? '100vw' : '70vw'
    });
    this.subscription = AddImageComponent.closed.subscribe(async () => {
      await this.ngOnInit();
      this.dialog.closeAll();
    });
  }

  onBrowserScroll($event: number): void {
    if (this.scrollableDiv.nativeElement.scrollTop < 10000) {
      this.scrollToBottom();
    }
  }

  scrollToBottom(): void {
    window.scroll({top: 10000, behavior: 'smooth'});
  }

  openCollectionDialog(): void {
    const addDialog = this.dialog.open(AddCollectionComponent, {});
    this.subscription = addDialog.componentInstance.submitted.subscribe(async () => {
      this.dialog.closeAll();
    });
  }

  async onImageClicked(fullImageParams: FullImageParams): Promise<void> {
    this.imageOverlayChild.imageId = fullImageParams.id;
    this.imageOverlayChild.enableButton = fullImageParams.enableButton;
    await this.imageOverlayChild.onChange();
    this.imageOverlayChild.show = true;
  }

}
