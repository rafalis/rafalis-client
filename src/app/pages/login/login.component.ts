import {Component, OnInit} from '@angular/core';
import {UntypedFormBuilder, UntypedFormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MobileService} from '../../services/mobile.service';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: UntypedFormGroup;
  returnUrl: string;
  loading = false;
  onReset = false;
  showCredits = true;

  constructor(private formBuilder: UntypedFormBuilder,
              private authService: AuthService,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private snackbar: MatSnackBar,
              readonly mobileService: MobileService,
              private titleService: Title) {
    this.loginForm = formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
    this.titleService.setTitle('Rafalis - Authentication');
  }

  ngOnInit(): void {
    this.returnUrl = this.activatedRoute.snapshot.queryParamMap.get('returnUrl') || '';
    if (this.authService.authenticated) {
      this.router.navigateByUrl('');
    }
  }

  async onSubmit(): Promise<void> {
    if (!this.loginForm.valid) {
      return;
    }
    this.loading = true;
    try {
      await this.authService.login(this.loginForm.controls.username.value, this.loginForm.controls.password.value);
      await this.router.navigateByUrl(decodeURI(this.returnUrl));
    } catch (error) {
      this.snackbar.open('Failed to login' + (error?.message ? ' : ' + error.message : ' : server unreachable'), 'OK');
    } finally {
      this.loading = false;
    }
  }

  onBrowseAnonymous(): void {
    this.router.navigateByUrl(decodeURI(this.returnUrl));
  }

  goToHome(): void {
    this.router.navigate(['']);
  }
}
