import {Component, HostListener, OnInit} from '@angular/core';
import {CollectionService} from '../../services/collection.service';
import {CollectionSummary} from '../../common/models/rafalis-model';
import {ImageService} from '../../services/image.service';
import {AuthService} from '../../services/auth.service';
import {MatDialog} from '@angular/material/dialog';
import {MobileService} from '../../services/mobile.service';
import {AddCollectionComponent} from '../add-collection/add-collection.component';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'app-collections',
  templateUrl: './collections.component.html',
  styleUrls: ['./collections.component.scss']
})
export class CollectionsComponent implements OnInit {

  collectionsPreview: CollectionSummary[] = [];
  mobileScreen: boolean;
  loading = false;

  @HostListener('contextmenu', ['$event'])
  onRightClick(event): void {
    event.preventDefault();
  }

  constructor(readonly collectionService: CollectionService,
              readonly imageService: ImageService,
              readonly auth: AuthService,
              private dialog: MatDialog,
              private readonly mobileService: MobileService,
              private titleService: Title) {
    this.titleService.setTitle('Rafalis - Collections');
    this.mobileScreen = this.mobileService.isMobileScreen;
  }

  async ngOnInit(): Promise<void> {
    this.loading = true;
    const summaries: CollectionSummary[] = await this.collectionService.getCollectionSummary();
    summaries.forEach((value => this.collectionsPreview.push(value)));
    this.collectionsPreview.sort((a, b) => a.name.localeCompare(b.name));
    this.loading = false;
  }

  newCollection(): void {
    const dialog = this.dialog.open(AddCollectionComponent, {});
    dialog.componentInstance.submitted.subscribe(async () => {
      this.dialog.closeAll();
      await this.ngOnInit();
    });
  }
}
