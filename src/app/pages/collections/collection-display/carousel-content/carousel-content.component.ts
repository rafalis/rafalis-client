import {Component, EventEmitter, Input, Output} from '@angular/core';
import {NgbCarouselConfig} from '@ng-bootstrap/ng-bootstrap';
import {ImageService} from '../../../../services/image.service';
import {MobileService} from '../../../../services/mobile.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-carousel-content',
  templateUrl: './carousel-content.component.html',
  styleUrls: ['./carousel-content.component.scss']
})
export class CarouselContentComponent {

  @Input() imageIds: number[];
  @Input() editable = false;
  @Output() modifyRequested = new EventEmitter<number[]>();
  @Output() deleteRequested = new EventEmitter<void>();

  zoomed: boolean = false;
  imageClass: string = 'carousel-image-small';
  mobile: boolean;

  constructor(config: NgbCarouselConfig,
              readonly imageService: ImageService,
              private readonly mobileService: MobileService,
              private router: Router) {
    config.keyboard = true;
    config.pauseOnHover = true;
    config.interval = 0;
    config.animation = true;
    this.mobile = this.mobileService.isMobileScreen;
  }

  onModify(): void {
    this.modifyRequested.emit(this.imageIds);
  }

  onDelete(): void {
    this.deleteRequested.emit();
  }

  toggleZoom(element): void {
    this.zoomed = !this.zoomed;
    this.imageClass = this.zoomed ? 'carousel-image' : 'carousel-image-small';
    if (this.zoomed) {
      document.querySelector(element).scrollIntoView({behavior: 'smooth', block: 'start'});
    }
  }

  openImage(id: number): void {
    const url = this.router.serializeUrl(this.router.createUrlTree([`/img`], {queryParams: {id: id}}));
    window.open(url, '_blank');
  }
}
