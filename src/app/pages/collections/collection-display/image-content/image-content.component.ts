import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {ImageService} from '../../../../services/image.service';
import {ImageDescription} from '../../../../common/models/rafalis-model';
import {MobileService} from '../../../../services/mobile.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-image-content',
  templateUrl: './image-content.component.html',
  styleUrls: ['./image-content.component.scss']
})
export class ImageContentComponent implements OnInit, OnChanges {

  @Input() id: number;
  @Input() editable = true;
  @Output() deleteEmitted = new EventEmitter();

  description: ImageDescription;
  mobile = false;
  zoomed = false;
  imageClass = 'collection-image-small';
  canShowImage = true;
  elementId: string;

  constructor(private readonly imageService: ImageService,
              private readonly mobileService: MobileService,
              private router: Router) {
  }

  async ngOnInit(): Promise<void> {
    this.elementId = '#me' + this.id;
    await this.getDescription();
    this.mobile = this.mobileService.isMobileScreen;
  }

  async ngOnChanges(changes: SimpleChanges): Promise<void> {
    if (changes.id?.isFirstChange()) {
      return;
    }
    if (changes.id?.previousValue !== changes.id?.currentValue && !isNaN(changes.id?.currentValue)) {
      await this.getDescription();
    }
  }

  onDelete(): void {
    this.deleteEmitted.emit();
  }

  async getDescription(): Promise<void> {
    try {
      this.description = await this.imageService.getImageDescription(this.id);
    } catch (e) {
      console.log('Description #' + this.id + ' not found : ' + e.message);
      this.canShowImage = false;
    }
    if (this.mobile) {
      return;
    }
    // automatically zoom panoramas if there are
    const items = await this.imageService.getList({color: '', publicOnly: false, keywords: this.description.name});
    if (items.length == 0 || items.length > 1) {
      console.log(`Unable to retrieve image #${this.description.id} dimensions, skipping.`);
      return;
    }
    if (items[0].minithumb_width > this.imageService.PANORAMA_THRESHOLD_MINITHUMB_PX) {
      this.imageClass = 'collection-image';
      this.zoomed = true;
    }
  }

  toggleZoom(element): void {
    this.zoomed = !this.zoomed;
    this.imageClass = this.zoomed ? 'collection-image' : 'collection-image-small';
    if (this.zoomed) {
      document.querySelector(element).scrollIntoView({behavior: 'smooth', block: 'start'});
    }
  }

  openImage(): void {
    const url = this.router.serializeUrl(this.router.createUrlTree([`/img`], {queryParams: {id: this.id}}));
    window.open(url, '_blank');
  }
}
