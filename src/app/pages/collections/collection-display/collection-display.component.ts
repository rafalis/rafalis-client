import {Component, EventEmitter, OnDestroy, OnInit} from '@angular/core';
import {Collection} from '../../../common/models/rafalis-model';
import {ActivatedRoute, Router} from '@angular/router';
import {CollectionService} from '../../../services/collection.service';
import {MatDialog} from '@angular/material/dialog';
import {ThumListComponent} from '../../browse/thum-list/thum-list.component';
import {MatSnackBar} from '@angular/material/snack-bar';
import {AuthService} from '../../../services/auth.service';
import {TextContentComponent} from './text-content/text-content.component';
import {UntypedFormBuilder, UntypedFormGroup, Validators} from '@angular/forms';
import {MobileService} from '../../../services/mobile.service';
import {ImageService} from '../../../services/image.service';
import {Title} from '@angular/platform-browser';


export enum CONTENT_TYPE {
  TEXT,
  IMAGE,
  CAROUSEL
}

@Component({
  selector: 'app-collection-display',
  templateUrl: './collection-display.component.html',
  styleUrls: ['./collection-display.component.scss']
})
export class CollectionDisplayComponent implements OnInit, OnDestroy {

  collection: Collection;
  id: number;
  modified = false;
  sendingChanges = false;
  structure: string[];
  indexesInContent = [];
  editable = false;
  saveAllEmitter = new EventEmitter();
  collectionForm: UntypedFormGroup;
  showAddersFlags = [];
  deleteConfirm = false;
  displayCollection = true;
  mobile: boolean;

  constructor(private readonly activatedRoute: ActivatedRoute,
              private readonly router: Router,
              private readonly collectionService: CollectionService,
              private dialog: MatDialog,
              private snackBar: MatSnackBar,
              readonly auth: AuthService,
              private formBuilder: UntypedFormBuilder,
              private imageService: ImageService,
              private readonly mobileService: MobileService,
              private titleService: Title) {
    this.collectionForm = this.formBuilder.group({
      name: ['', Validators.required],
      description: ['', Validators.required],
      thumbnailid: ['', Validators.required],
      ispublic: ['']
    });
  }

  async ngOnInit(): Promise<void> {
    this.mobile = this.mobileService.isMobileScreen;
    this.activatedRoute.paramMap.subscribe(value => this.id = +value.get('coll-id'));
    if (isNaN(this.id)) {
      await this.router.navigateByUrl('/collections');
      return;
    }
    await this.initCollection();
  }

  async ngOnDestroy(): Promise<void> {
    if (!this.editable) {
      return;
    }
    await this.toggleEditable(false);
    this.snackBar.open('Saved "' + this.collection.name + '" changes.', '',
      {duration: 5000, verticalPosition: 'top', horizontalPosition: 'end'});
  }

  private updateCarousel(newIds: number[], index: number): void {
    this.collection.carouselContent[this.indexesInContent[index]] = newIds;
  }

  private addText(index: number, content: string = ''): void {
    if (index === -1) {
      this.collection.structure += 'T';
      this.updateIndexesInContent();
      this.collection.textContent.push(content);
    } else {
      this.collection.structure = this.collection.structure.slice(0, index) + 'T' + this.collection.structure.slice(index);
      this.updateIndexesInContent();
      this.collection.textContent.splice(this.indexesInContent[index], 0, content);
    }
    this.structure = this.strToArray(this.collection.structure);
    this.modified = true;
  }

  private addImage(id: number, index: number): void {
    if (index === -1) {
      this.collection.structure += 'I';
      this.updateIndexesInContent();
      this.collection.imageContent.push(id);
    } else {
      this.collection.structure = this.collection.structure.slice(0, index) + 'I' + this.collection.structure.slice(index);
      this.updateIndexesInContent();
      this.collection.imageContent.splice(this.indexesInContent[index], 0, id);
    }
    this.structure = this.strToArray(this.collection.structure);
    this.modified = true;
    this.dialog.closeAll();

    const snack = this.snackBar.open('Image added', 'Add title as caption', {duration: 8000});
    snack.onAction().subscribe(async () => {
      const description = await this.imageService.getImageDescription(id);
      this.addText(index === -1 ? -1 : index + 1, `<h5 align="center">${description.title}</h5>`);
    });
  }

  private addCarousel(ids: number[], index: number): void {
    if (index === -1) {
      this.collection.structure += 'C';
      this.updateIndexesInContent();
      this.collection.carouselContent.push(ids);
    } else {
      this.collection.structure = this.collection.structure.slice(0, index) + 'C' + this.collection.structure.slice(index);
      this.updateIndexesInContent();
      this.collection.carouselContent.splice(this.indexesInContent[index], 0, ids);
    }
    this.structure = this.strToArray(this.collection.structure);
    this.modified = true;
    this.dialog.closeAll();
  }

  private selecetNewImageForImage(index: number): void {
    const dialog = this.dialog.open(ThumListComponent, {
      width: this.mobileService.isMobileScreen ? '95vw' : '80vw'
    });
    const instance = dialog.componentInstance;
    instance.collectionAddMode = true;
    instance.selectionConfirmed.subscribe((async (value) => {
      if (!value[0].ispublic && this.collectionForm.value.ispublic) {
        this.snackBar.open('The image you selected was private, and this collection is public. Either set this image' +
          ' to public, or this collection to private. Hover an image to see if it is public.', 'Got it', {});
        return;
      }
      this.addImage(value[0].id, index);
    }));
  }

  private selectNewImagesForCarousel(index: number): void {
    const dialog = this.dialog.open(ThumListComponent, {
      width: this.mobileService.isMobileScreen ? '95vw' : '80vw'
    });
    const instance = dialog.componentInstance;
    instance.collectionAddMode = true;
    instance.multipleMode = true;
    instance.selectionConfirmed.subscribe((async (imageItems) => {
      for (const imageItem of imageItems) {
        if (!imageItem.ispublic && this.collectionForm.value.ispublic) {
          this.snackBar.open('One of the images you selected was private, and this collection is public. Either set this image' +
            ' to public, or this collection to private. Hover an image to see if it is public.', 'Got it', {});
          return;
        }
      }
      this.addCarousel(imageItems.map(item => item.id), index);
    }));
  }

  deleteContentAtIndex(index: number, type: CONTENT_TYPE): void {
    if (type === CONTENT_TYPE.TEXT) {
      // doing all this instead of splice() in order to make angular detect that the array changed
      const newTextContent = [];
      this.collection.textContent.forEach((value, index1) => {
        if (index1 !== this.indexesInContent[index]) {
          newTextContent.push(value);
        }
      });
      this.collection.textContent = newTextContent;
    } else if (type === CONTENT_TYPE.IMAGE) {
      // doing all this instead of splice() in order to make angular detect that the array changed
      const newImageContent = [];
      this.collection.imageContent.forEach((value, index1) => {
        if (index1 !== this.indexesInContent[index]) {
          newImageContent.push(value);
        }
      });
      this.collection.imageContent = newImageContent;
    } else if (type === CONTENT_TYPE.CAROUSEL) {
      // doing all this instead of splice() in order to make angular detect that the array changed
      const newCarouselContent = [];
      this.collection.carouselContent.forEach((value, index1) => {
        if (index1 !== this.indexesInContent[index]) {
          newCarouselContent.push(value);
        }
      });
      this.collection.carouselContent = newCarouselContent;
    }
    this.collection.structure = this.collection.structure.slice(0, index) + this.collection.structure.slice(index + 1);
    this.structure = this.strToArray(this.collection.structure);
    this.updateIndexesInContent();
    this.modified = true;
  }

  private updateIndexesInContent(): void {
    let textIndex = 0;
    let imageIndex = 0;
    let carouselIndex = 0;
    this.indexesInContent = [];
    this.strToArray(this.collection.structure).forEach(value => {
      if (value === 'T') {
        this.indexesInContent.push(textIndex++);
      } else if (value === 'I') {
        this.indexesInContent.push(imageIndex++);
      } else if (value === 'C') {
        this.indexesInContent.push(carouselIndex++);
      }
    });
  }

  private strToArray(struct: string): string[] {
    const ar = [];
    for (const char of struct) {
      ar.push(char);
    }
    return ar;
  }

  onThumbnailChooseButtonClick(): void {
    const dialog = this.dialog.open(ThumListComponent, {
      width: this.mobileService.isMobileScreen ? '95vw' : '80vw'
    });
    const instance = dialog.componentInstance;
    instance.collectionAddMode = true;
    instance.selectionConfirmed.subscribe((value => {
      this.collection.thumbnailid = value[0].id;
      this.modified = true;
      this.dialog.closeAll();
    }));
  }

  private async postCollection(): Promise<void> {
    this.sendingChanges = true;
    try {
      await this.collectionService.modifyCollection(this.collection);
      this.snackBar.open('Changes successfuly saved !', '', {duration: 3000});
    } catch (e) {
      this.snackBar.open('Sorry, failed to save changes.', '', {duration: 5000, verticalPosition: 'top'});
      throw e;
    } finally {
      this.sendingChanges = false;
    }
    this.sendingChanges = false;
  }

  async toggleEditable(cancel: boolean = false): Promise<void> {
    if (this.editable) {
      this.modified = false;
      if (cancel) {
        this.collection = null;
        await this.initCollection();
      } else {
        this.saveAllEmitter.emit();
        this.collection.description = this.collectionForm.value.description;
        this.collection.name = this.collectionForm.value.name;
        this.collection.ispublic = this.collectionForm.value.ispublic;
        await this.postCollection();
      }
    }
    this.editable = !this.editable;
  }

  saveTextFromTextSection(newText: { indexInStructure: number, content: string }): void {
    if (newText.content === TextContentComponent.DELETE_MESSAGE) {
      this.deleteContentAtIndex(newText.indexInStructure, CONTENT_TYPE.TEXT);
      return;
    }
    this.collection.textContent[this.indexesInContent[newText.indexInStructure]] = newText.content;
    this.updateIndexesInContent();
    this.modified = true;
  }

  private async initCollection(): Promise<void> {
    try {
      this.collection = await this.collectionService.getCollection(this.id);
    } catch (error) {
      if (error.code === 404) {
        this.displayCollection = false;
      } else if (error.code === 403) {
        this.auth.loginBeforeAccessingContent();
      } else {
        this.snackBar.open('Failed to retrieve the collection.', '', {duration: 5000, verticalPosition: 'top'});
      }
      return;
    }
    this.structure = this.strToArray(this.collection.structure);
    this.updateIndexesInContent();
    this.collectionForm.patchValue({
      name: this.collection.name,
      description: this.collection.description,
      thumnailid: this.collection.thumbnailid,
      ispublic: this.collection.ispublic
    });
    this.showAddersFlags = new Array<boolean>(this.collection.structure.length);
    this.titleService.setTitle('Rafalis - ' + this.collection.name);
  }

  async deleteCollection(): Promise<void> {
    try {
      await this.collectionService.deleteCollection(this.collection.id);
      this.displayCollection = false;
      this.snackBar.open('Collection successfully deleted', 'OK', {duration: 5000});
    } catch (error) {
      this.snackBar.open('Failed to delete collection.', '', {verticalPosition: 'top', duration: 5000});
    }
  }

  modifyRequested(type: CONTENT_TYPE, index: number): void {
    if (type === CONTENT_TYPE.TEXT) {
      this.addText(index);
    } else if (type === CONTENT_TYPE.IMAGE) {
      this.selecetNewImageForImage(index);
    } else if (type === CONTENT_TYPE.CAROUSEL) {
      this.selectNewImagesForCarousel(index);
    }
  }

  onCarouselModify(currentIds: number[], indexInCollection: number): void {
    const dialog = this.dialog.open(ThumListComponent, {
      width: this.mobileService.isMobileScreen ? '95vw' : '80vw',
      data: {selectedIds: currentIds}
    });
    const instance = dialog.componentInstance;
    instance.collectionAddMode = true;
    instance.multipleMode = true;
    instance.selectionConfirmed.subscribe((async (imageItems) => {
      for (const imageItem of imageItems) {
        if (!imageItem.ispublic && this.collectionForm.value.ispublic) {
          this.snackBar.open('One of the images you selected was private, and this collection is public. Either set this image' +
            ' to public, or this collection to private. Hover an image to see if it is public.', 'Got it', {});
          return;
        }
      }
      this.updateCarousel(imageItems.map(item => item.id), indexInCollection);
      dialog.close();
    }));
  }

}
