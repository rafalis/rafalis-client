import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {AngularEditorConfig} from '@kolkov/angular-editor';

@Component({
  selector: 'app-text-content',
  templateUrl: './text-content.component.html',
  styleUrls: ['./text-content.component.scss']
})
export class TextContentComponent implements OnInit, OnChanges {

  static DELETE_MESSAGE = 'delete_me_now';

  @Input() saveAll;
  @Input() editable = true;
  @Input() currentHTML: string;
  @Output() newTextEmitter = new EventEmitter<string>();
  localEdit = false;
  htmlContent = '';

  config: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    sanitize: true,
    fonts: [
      {class: 'times-new-roman', name: 'Times New Roman'},
      {class: 'roboto', name: 'Roboto'},
    ],
    minHeight: '5rem',
    placeholder: 'Enter text here...',
    translate: 'no',
    defaultParagraphSeparator: 'p',
    defaultFontName: 'Roboto',
    toolbarHiddenButtons: [['insertVideo', 'insertImage', 'clearFormatting', 'insertHorizontalRule']],
  };

  constructor() {
  }

  ngOnInit(): void {
    if (this.currentHTML) {
      this.htmlContent = this.currentHTML;
    }
    this.saveAll.subscribe(() => {
      this.newTextEmitter.emit(this.htmlContent);
      this.localEdit = false;
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.currentHTML?.isFirstChange()) {
      return;
    }
    if (changes.currentHTML?.previousValue !== changes.currentHTML?.currentValue) {
      this.htmlContent = this.currentHTML;
    }
  }

  onDelete(): void {
    this.newTextEmitter.emit(TextContentComponent.DELETE_MESSAGE);
  }

  enablePreview(): void {
    this.localEdit = true;
    this.editable = false;
  }

  onEdit(): void {
    this.editable = true;
    this.localEdit = false;
  }

}
