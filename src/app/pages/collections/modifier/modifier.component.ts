import {Component, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'app-modifier',
  templateUrl: './modifier.component.html',
  styleUrls: ['./modifier.component.scss']
})
export class ModifierComponent {

  @Output() request = new EventEmitter<number>();

  constructor() {
  }

}
