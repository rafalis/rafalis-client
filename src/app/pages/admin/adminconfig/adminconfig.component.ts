import {Component, OnInit} from '@angular/core';
import {UntypedFormBuilder, UntypedFormGroup} from '@angular/forms';
import {AdminConfig} from '../../../common/models/rafalis-model';
import {AppConfigService} from '../../../services/app-config.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MobileService} from '../../../services/mobile.service';

@Component({
  selector: 'app-adminconfig',
  templateUrl: './adminconfig.component.html',
  styleUrls: ['./adminconfig.component.scss']
})
export class AdminconfigComponent implements OnInit {

  adminConfigForm: UntypedFormGroup;
  adminConfig: AdminConfig;
  uploading: boolean = false;
  newCameraAdded = false;
  newAuthorAdded = false;
  isMobileScreen = false;

  constructor(private fb: UntypedFormBuilder,
              private appConfigService: AppConfigService,
              private snackbar: MatSnackBar,
              private mobileService: MobileService) {
    this.adminConfigForm = this.fb.group({
      newAuthor: [''],
      newCamera: [''],
      newMainTitle: [''],
      newSubTitle: [''],
    });
    this.isMobileScreen = this.mobileService.isMobileScreen;
  }

  async ngOnInit(): Promise<void> {
    this.adminConfig = await this.appConfigService.getConfig();
    this.adminConfigForm.patchValue({newMainTitle: this.adminConfig.mainTitle});
    this.adminConfigForm.patchValue({newSubTitle: this.adminConfig.subTitle});
  }

  async onSubmit(): Promise<void> {
    this.uploading = true;
    try {
      await this.appConfigService.modifyConfig(this.adminConfig);
      this.snackbar.open('Successfully updated app configuration.', '', {duration: 2000});
      this.appConfigService.invalidateConfig();
      this.appConfigService.ensureConfigIsRetrieved();
    } catch (e) {
      this.snackbar.open('Failed to update config.', '', {duration: 5000, verticalPosition: 'top'});
    } finally {
      this.uploading = false;
      this.adminConfigForm.patchValue({
        newAuthor: '',
        newCamera: '',
        newMainTitle: '',
        newSubTitle: '',
      });
    }
  }

  addAuthor(): void {
    if (!this.adminConfigForm.value.newAuthor || this.newAuthorAdded) {
      return;
    }
    this.adminConfig.authors.push(this.adminConfigForm.value.newAuthor);
    this.adminConfigForm.patchValue({newAuthor: ''});
    this.newAuthorAdded = true;
    setTimeout(() => this.newAuthorAdded = false, 1000);
  }

  addCamera(): void {
    if (!this.adminConfigForm.value.newCamera || this.newCameraAdded) {
      return;
    }
    this.adminConfig.cameras.push(this.adminConfigForm.value.newCamera);
    this.adminConfigForm.patchValue({newCamera: ''});
    this.newCameraAdded = true;
    setTimeout(() => this.newCameraAdded = false, 1000);
  }

  removeAuthor(author: string): void {
    const index = this.adminConfig.authors.indexOf(author);
    if (index !== -1) {
      this.adminConfig.authors.splice(index, 1);
    }
  }

  removeCamera(camera: string): void {
    const index = this.adminConfig.cameras.indexOf(camera);
    if (index !== -1) {
      this.adminConfig.cameras.splice(index, 1);
    }
  }

  mainTitleChanged(): void {
    this.adminConfig.mainTitle = this.adminConfigForm.value.newMainTitle;
  }

  subTitleChanged(): void {
    this.adminConfig.subTitle = this.adminConfigForm.value.newSubTitle;
  }
}
