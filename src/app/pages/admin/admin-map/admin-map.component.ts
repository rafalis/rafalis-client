import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {MapSummary} from '../../../common/models/rafalis-model';
import {MapService} from '../../../services/map.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MobileService} from '../../../services/mobile.service';
import {MatDialog} from '@angular/material/dialog';
import {MapPlacerDialogComponent} from '../../../common/map-placer-dialog/map-placer-dialog.component';

@Component({
  selector: 'app-admin-map',
  templateUrl: './admin-map.component.html',
  styleUrls: ['./admin-map.component.scss']
})
export class AdminMapComponent implements OnInit {

  @Output() mapDeletedEmitted = new EventEmitter<Set<number>>();

  updateMaps = new EventEmitter<void>();
  maps: MapSummary[];
  currentSelectedMap: MapSummary;
  mapsIDToDelete = new Set<number>();
  isConfiguratorReadonly = false;

  constructor(readonly mapService: MapService,
              private snackbar: MatSnackBar,
              readonly mobileService: MobileService,
              private dialog: MatDialog) {
  }

  async ngOnInit(): Promise<void> {
    await this.getData();
    this.updateMaps.subscribe(() => this.getData());
  }

  async getData(): Promise<void> {
    this.maps = await this.mapService.getMapSummaries();
    this.maps = this.mapService.orderMaps(this.maps);
    this.currentSelectedMap = this.maps[0];
    this.updateReadOnly();
  }

  updateReadOnly(): void {
    this.isConfiguratorReadonly = this.currentSelectedMap.filename === MapService.RESERVED_FILENAME;
  }

  onMapDelete(id: number): void {
    this.mapsIDToDelete.add(id);
    this.mapDeletedEmitted.emit(this.mapsIDToDelete);
    this.currentSelectedMap = null;
  }

  openEditor(): void {
    const dialog = this.dialog.open(MapPlacerDialogComponent, {
      data: {imageDescription: null, mapID: this.currentSelectedMap.id, openedFromAdmin: true},
      width: this.mobileService.isMobileScreen ? '100vw' : '90vw'
    });
    dialog.componentInstance.changesSaved.subscribe(() => dialog.close());
  }

}
