import {Component, OnInit, ViewChild} from '@angular/core';
import {CollectionSummary, ImageItem} from '../../common/models/rafalis-model';
import {ImageService} from '../../services/image.service';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {CollectionService} from '../../services/collection.service';
import {MapConfiguratorComponent} from '../../common/map/map-configurator/map-configurator.component';
import {AddCollectionComponent} from '../add-collection/add-collection.component';
import {AddImageComponent} from '../add-image/add-image.component';
import {MobileService} from '../../services/mobile.service';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';
import {MapService} from '../../services/map.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatSlideToggleChange} from '@angular/material/slide-toggle';
import {AdminMapComponent} from './admin-map/admin-map.component';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  @ViewChild('sortImages') sortImages: MatSort;
  imageDescriptions: ImageItem[];
  imageDisplayedColumns = ['id', 'title', 'preview', 'datetime', 'hide_in_browse', 'homepage', 'ispublic', 'delete', 'open'];
  imageDataSource: MatTableDataSource<ImageItem>;
  imagesToDelete = new Set<ImageItem>();
  collectionsToDelete = new Set<CollectionSummary>();
  updatingFieldImage = false;

  @ViewChild('sortCollections') sortCollections: MatSort;
  collections: CollectionSummary[];
  collectionDisplayedColumns = ['id', 'name', 'description', 'thumbnail', 'ispublic', 'delete', 'open'];
  collectionDataSource: MatTableDataSource<CollectionSummary>;
  updatingFieldCollection = false;

  @ViewChild('mapAdminComponent') mapAdminComponent: AdminMapComponent;
  mapsIDToDelete = new Set<number>();

  isMobileScreen = false;

  private newCollectionDialogRef: MatDialogRef<AddCollectionComponent>;

  constructor(readonly imageService: ImageService,
              private collectionService: CollectionService,
              private mobileService: MobileService,
              private dialog: MatDialog,
              private router: Router,
              readonly authService: AuthService,
              private mapService: MapService,
              private snackbar: MatSnackBar,
              private titleService: Title) {
    this.titleService.setTitle('Rafalis - Creator panel');
    this.isMobileScreen = this.mobileService.isMobileScreen;
  }

  async ngOnInit(): Promise<void> {
    await this.getData();
    this.imageDataSource.sort.sort({id: 'id', start: 'desc', disableClear: true});
    AddImageComponent.closed.subscribe(() => this.onDialogClose());
    this.mapService.closed.subscribe(() => this.onDialogClose());
  }

  private async onDialogClose(): Promise<void> {
    this.dialog.closeAll();
    await this.getData();
  }

  async getData(): Promise<void> {
    this.imageDescriptions = await this.imageService.getList({publicOnly: false, keywords: '', color: ''});
    this.collections = await this.collectionService.getCollectionSummary();

    this.imageDataSource = new MatTableDataSource<ImageItem>(this.imageDescriptions);
    this.imageDataSource.sort = this.sortImages;
    this.imageDataSource.sortingDataAccessor = (item, property): any => {
      switch (property) {
        case 'hide_in_browse':
          return item.show_in_browse;
        case 'homepage':
          return item.show_in_home;
        default:
          return item[property];
      }
    };
    this.collectionDataSource = new MatTableDataSource<CollectionSummary>(this.collections);
    this.collectionDataSource.sort = this.sortCollections;
  }

  async applyChanges(): Promise<void> {
    if (!this.imagesToDelete.size && !this.collectionsToDelete.size && !this.mapsIDToDelete.size) {
      this.snackbar.open('Nothing to do.', '', {duration: 2000, verticalPosition: 'top'});
      return;
    }
    for (const image of this.imagesToDelete) {
      try {
        await this.imageService.deleteImage(image.id);
      } catch (e) {
        if (e.code === 400) {
          this.snackbar.open(`Skipping deletion of image #${image.id} "${image.title}" because it appears in at least 1 collection. Delete it from there first !`,
            '',
            {duration: 10000});
        } else {
          this.snackbar.open(`Failed to delete image #${image.id} "${image.title}".`);
        }
      }
    }
    for (const collection of this.collectionsToDelete) {
      await this.collectionService.deleteCollection(collection.id);
    }
    for (const mapid of this.mapsIDToDelete) {
      await this.mapService.deleteMap(mapid);
    }
    this.imagesToDelete.clear();
    this.collectionsToDelete.clear();
    this.mapsIDToDelete.clear();
    await this.getData();
    this.mapAdminComponent.updateMaps.emit();
  }

  openNewMapDialog(): void {
    this.dialog.open(MapConfiguratorComponent, {
      data: {},
      height: '50vh',
      width: this.mobileService.isMobileScreen ? '100vw' : '30vw'
    });
  }

  onMapDelete(newMapIds: Set<number>): void {
    this.mapsIDToDelete = newMapIds;
  }

  openAddDialog(): void {
    this.dialog.open(AddImageComponent, {
      data: {mobile: this.mobileService.isMobileScreen},
      width: this.mobileService.isMobileScreen ? '100vw' : '80vw'
    });
  }

  openNewCollectionDialog(): void {
    this.newCollectionDialogRef = this.dialog.open(AddCollectionComponent, {});
    this.newCollectionDialogRef.componentInstance.submitted.subscribe(async () => {
      this.newCollectionDialogRef.close();
      await this.router.navigate(['collections']);
    });
  }

  async onToggleChangeCollection($event: MatSlideToggleChange, id: number): Promise<void> {
    this.updatingFieldCollection = true;
    try {
      const collection = await this.collectionService.getCollection(id);
      collection.ispublic = $event.checked;
      await this.collectionService.modifyCollection(collection);
    } catch (e) {
      this.snackbar.open('Failed to update public attribute : ' + e.message, 'OK');
    } finally {
      this.updatingFieldCollection = false;
    }
    this.updatingFieldCollection = false;
  }

  async onToggleChangeImage($event: MatSlideToggleChange, id: number, publicToggle: TOGGLE_TYPE): Promise<void> {
    this.updatingFieldImage = true;
    try {
      const description = await this.imageService.getImageDescription(id);
      switch (publicToggle) {
        case TOGGLE_TYPE.HIDDEN:
          description.show_in_browse = !$event.checked;
          break;
        case TOGGLE_TYPE.PUBLIC:
          description.ispublic = $event.checked;
          break;
        case TOGGLE_TYPE.HOMEPAGE:
          description.show_in_home = $event.checked;
          break;
      }
      await this.imageService.updateDescription(id, description);
    } catch (e) {
      this.snackbar.open('Failed to update attribute : ' + e.message, 'OK');
    } finally {
      this.updatingFieldImage = false;
    }
    this.updatingFieldImage = false;
  }

  openImage(id: number): void {
    const url = this.router.serializeUrl(this.router.createUrlTree([`/img`], {queryParams: {id: id}}));
    window.open(url, '_blank');
  }

  openCollection(id: number): void {
    const url = this.router.serializeUrl(this.router.createUrlTree([`/collections/${id}`], {}));
    window.open(url, '_blank');
  }

  applyIsEnabled(): boolean {
    return (this.imagesToDelete.size > 0 || this.collectionsToDelete.size > 0 || this.mapsIDToDelete.size > 0);
  }
}

enum TOGGLE_TYPE {
  PUBLIC,
  HIDDEN,
  HOMEPAGE
}
