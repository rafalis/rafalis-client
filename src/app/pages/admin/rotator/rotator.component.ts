import {Component} from '@angular/core';
import {ImageService} from '../../../services/image.service';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-rotator',
  templateUrl: './rotator.component.html',
  styleUrls: ['./rotator.component.scss']
})
export class RotatorComponent {

  rotation: number;
  imageID: number;
  qualities = [];
  rotationOptions = [];
  uploading: boolean = false;

  constructor(private imageService: ImageService,
              private snackbar: MatSnackBar) {
    this.rotationOptions.push({value: 90, display: '90°'});
    this.rotationOptions.push({value: 180, display: '180°'});
    this.rotationOptions.push({value: 270, display: '270°'});
  }

  async onSubmit(): Promise<void> {
    this.uploading = true;
    try {
      const message = await this.imageService.rotateImage(this.imageID, this.rotation, [1, 2, 3]);
      if (message.message === 'success') {
        this.snackbar.open('Successfully rotated image #' + this.imageID + ' by ' + this.rotation + '°',
          'OK', {duration: 2000});
      } else {
        this.snackbar.open('Error while rotating image.', '', {duration: 5000, verticalPosition: 'top'});
      }
    } catch (e) {
      this.snackbar.open('Error while rotating image.', '', {duration: 5000, verticalPosition: 'top'});
    }
    this.uploading = false;
  }
}
