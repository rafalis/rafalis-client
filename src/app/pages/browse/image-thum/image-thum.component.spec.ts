import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImageThumComponent } from './image-thum.component';

describe('ImageThumComponent', () => {
  let component: ImageThumComponent;
  let fixture: ComponentFixture<ImageThumComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ImageThumComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageThumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
