import {AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {IMAGE_QUALITY, ImageItem} from '../../../common/models/rafalis-model';
import {ImageService} from '../../../services/image.service';
import {AuthService} from '../../../services/auth.service';
import {SafeUrl} from "@angular/platform-browser";

@Component({
  selector: 'app-image-thum',
  templateUrl: './image-thum.component.html',
  styleUrls: ['./image-thum.component.scss']
})
export class ImageThumComponent implements OnInit, AfterViewInit {
  @ViewChild('theImage') theImage: ElementRef;
  @Input() imageItem: ImageItem;
  @Input() knapsackWidth: number;
  @Input() selected: boolean;
  @Input() collectionAddMode: boolean;
  thumbnailurl: Promise<SafeUrl>;
  imageHeight: number;
  showImage = false;

  constructor(private imageService: ImageService,
              readonly auth: AuthService) {
  }

  ngOnInit(): void {
    this.thumbnailurl = this.getImageURL(this.imageItem);
    this.imageHeight = this.imageItem.thumb_height / this.imageItem.thumb_width * this.knapsackWidth;
  }

  ngAfterViewInit(): void {
    this.theImage.nativeElement.onload = (): void => {
      this.showImage = true;
    };
  }

  async getImageURL(item: ImageItem): Promise<SafeUrl | string> {
    if (item.ispublic) {
      return this.imageService.thumbNailUrlFromId(item.id);
    } else {
      const res = await this.imageService.getImageBytes(item.id, IMAGE_QUALITY.THUMB_QUALITY);
      return this.imageService.createImageFromBlob(res.body);
    }
  }

  getInfos(): string {
    let infos = `${this.imageItem.title} [#${this.imageItem.id}]${this.imageItem.datetime ? `\n${this.imageItem.datetime}` : ''}`;
    if (this.auth.isAdmin) {
      infos += (this.imageItem.ispublic ? '\nPublic.' : '\nPrivate.');
    }
    return infos;
  }

}
