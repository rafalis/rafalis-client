import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ThumListComponent} from './thum-list.component';

describe('ThumListComponent', () => {
  let component: ThumListComponent;
  let fixture: ComponentFixture<ThumListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ThumListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ThumListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
