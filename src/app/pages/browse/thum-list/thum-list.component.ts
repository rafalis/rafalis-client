import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Inject,
  Input,
  OnDestroy,
  OnInit,
  Optional,
  Output,
  ViewChild
} from '@angular/core';
import {FullImageParams, ImageItem} from '../../../common/models/rafalis-model';
import {ImageService} from '../../../services/image.service';
import {FiltersService} from '../../../services/filters.service';
import {fromEvent, Subscription} from 'rxjs';
import {ScrollService} from '../../../services/scroll.service';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {tap, throttleTime} from 'rxjs/operators';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {SORT_BY} from '../../../common/filters/filters.component';
import {AuthService} from "../../../services/auth.service";
import {NgxMasonryComponent, NgxMasonryOptions} from "ngx-masonry";
import {MobileService} from "../../../services/mobile.service";


export interface ThumListData {
  selectedIds: number[];
}

@Component({
  selector: 'app-thum-list',
  templateUrl: './thum-list.component.html',
  styleUrls: ['./thum-list.component.scss'],
  animations: [
    trigger('scrollToTop', [
      state('show', style({
        opacity: 1,
      })),
      state('dontshow', style({
        opacity: 0,
        display: 'none'
      })),
      transition('show => dontshow', [
        animate('0.4s')
      ]),
      transition('dontshow => show', [
        animate('0s')
      ]),
    ]),
  ]
})
export class ThumListComponent implements OnInit, OnDestroy, AfterViewInit {

  @ViewChild('scrollablediv') scrollableDiv: ElementRef;
  @ViewChild(NgxMasonryComponent) masonry: NgxMasonryComponent;
  @Input() collectionAddMode = false;
  @Input() multipleMode = false;
  @Output() selectionConfirmed = new EventEmitter<ImageItem[]>();
  @Output() isUserScrollingDown = new EventEmitter<number>();
  @Output() imageClicked = new EventEmitter<FullImageParams>();
  private knapsackItemsLoaded = false;
  imageItemsToDisplay: ImageItem[];
  publicImageItemsToDisplay: ImageItem[];
  showOnlyPublic = false;
  loading = true;
  observables: Subscription[] = [];
  browseScrollTop: number;
  displayScrollToTop = false;
  selectedImages: Set<ImageItem> = new Set<ImageItem>();
  currentSortType: SORT_BY;

  knapsackWidth = 300;
  options: NgxMasonryOptions;

  constructor(public imageService: ImageService,
              private authService: AuthService,
              private filterService: FiltersService,
              readonly scrollService: ScrollService,
              private mobileService: MobileService,
              @Optional() @Inject(MAT_DIALOG_DATA) public thumListData: ThumListData) {
    this.currentSortType = this.filterService.sortbyStr;
  }

  async ngOnInit(): Promise<void> {
    this.showOnlyPublic = this.filterService.filters.publicOnly;
    const allItemsSubscription = this.imageService.emitter.subscribe((allNewItems: ImageItem[]) => {
      allNewItems = allNewItems.filter(item => item.show_in_browse || this.collectionAddMode);
      this.imageItemsToDisplay = allNewItems;
      this.publicImageItemsToDisplay = this.imageItemsToDisplay.filter(value => value.ispublic);
      this.filterService.updateAvailableTags(allNewItems); // will filter public items itself
      this.loading = false;
      this.sort();
      if (this.collectionAddMode && this.thumListData?.selectedIds.length > 0) {
        this.thumListData.selectedIds.forEach(value => this.selectedImages.add(this.imageItemsToDisplay.find(item => item.id === value)));
      }
    });
    this.observables.push(allItemsSubscription);

    const publicItemsSubscription = this.filterService.publicToggleEmitter.subscribe((newVal) => {
      this.showOnlyPublic = newVal;
      this.sort();
    });
    this.observables.push(publicItemsSubscription);

    const sortbySubscription = this.filterService.newSortByEmitter.subscribe((newSortBy) => {
      this.currentSortType = newSortBy;
      this.sort();
    });
    this.observables.push(sortbySubscription);
    const masonryWidth = this.collectionAddMode ? innerWidth * 0.8 : innerWidth;
    this.knapsackWidth = this.mobileService.isMobileScreen ? (masonryWidth - 24) / 3 : (masonryWidth - 24) / 5;
    this.options = {
      itemSelector: '.knapsack-item',
      columnWidth: '.knapsack-grid-sizer',
      percentPosition: true,
      fitWidth: true,
      gutter: '.knapsack-gutter-sizer',
    };
  }

  private sort(): void {
    let newItems = [];
    let newPublicItems = [];
    if (!this.imageItemsToDisplay || !this.publicImageItemsToDisplay) {
      return;
    }
    switch (this.currentSortType) {
      case SORT_BY.RECENT_ASC:
        this.imageItemsToDisplay.sort((a, b) =>
          a.id - b.id).forEach(value => newItems.push(value));
        this.publicImageItemsToDisplay.sort((a, b) =>
          a.id - b.id).forEach(value => newPublicItems.push(value));
        break;
      case SORT_BY.RECENT_DESC:
        this.imageItemsToDisplay.sort((a, b) =>
          b.id - a.id).forEach(value => newItems.push(value));
        this.publicImageItemsToDisplay.sort((a, b) =>
          b.id - a.id).forEach(value => newPublicItems.push(value));
        break;
      case SORT_BY.TITLE_ASC:
        this.imageItemsToDisplay.sort((a, b) =>
          b.title.localeCompare(a.title)).forEach(value => newItems.push(value));
        this.publicImageItemsToDisplay.sort((a, b) =>
          b.title.localeCompare(a.title)).forEach(value => newPublicItems.push(value));
        break;
      case SORT_BY.TITLE_DESC:
        this.imageItemsToDisplay.sort((a, b) =>
          a.title.localeCompare(b.title)).forEach(value => newItems.push(value));
        this.publicImageItemsToDisplay.sort((a, b) =>
          a.title.localeCompare(b.title)).forEach(value => newPublicItems.push(value));
        break;
      default:
        const ids = this.imageService.idsToDisplay;
        const order = {};
        ids.forEach((id, i) => order[id] = i);
        this.imageItemsToDisplay.sort((a, b) => order[a.id] - order[b.id])
          .forEach(value => newItems.push(value));
        this.imageItemsToDisplay.sort((a, b) => order[a.id] - order[b.id])
          .filter(value => value.ispublic)
          .forEach(value => newItems.push(value));
        return;
    }
    this.imageItemsToDisplay = newItems;
    this.publicImageItemsToDisplay = newPublicItems;
    if (this.showOnlyPublic) {
      this.imageService.updateImageItemsOrder(newPublicItems);
    } else {
      this.imageService.updateImageItemsOrder(newItems);
    }
    /* only reload when items are already all loaded */
    if (this.knapsackItemsLoaded) {
      this.masonry.reloadItems();
      this.masonry.layout();
    }
  }

  ngOnDestroy(): void {
    this.observables.forEach(value => value.unsubscribe());
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.observables.push(fromEvent(this.scrollableDiv.nativeElement, 'scroll')
        .pipe(
          throttleTime(100),
          tap(event => this.onDivScroll(event as Event))
        ).subscribe());
    }, 400);
    this.scrollBackToWhereYouWere();
    const masonrySubscription = this.masonry.itemsLoaded.subscribe(() => {
      this.knapsackItemsLoaded = true;
    });
    this.observables.push(masonrySubscription);
  }

  private scrollBackToWhereYouWere(): void {
    if (this.loading) {
      setTimeout(() => this.scrollBackToWhereYouWere(), 500);
    } else {
      if (this.scrollService.scrollBackEnabled && this.scrollService.browseScrollY > 100) {
        this.browseScrollTop = this.scrollService.browseScrollY;
        this.scrollService.disableScrollBack();
      }
    }
  }

  async onImageClicked(imageItem: ImageItem): Promise<void> {
    if (this.collectionAddMode) {
      if (this.multipleMode) {
        if (this.selectedImages.has(imageItem)) {
          this.selectedImages.delete(imageItem);
        } else {
          this.selectedImages.add(imageItem);
        }
      } else {
        this.selectionConfirmed.emit([imageItem]);
      }
    } else {
      let config: FullImageParams = {
        id: imageItem.id,
        enableButton: true
      };
      this.imageClicked.emit(config);
    }
  }

  onConfirmSelection(): void {
    this.selectionConfirmed.emit(Array.from(this.selectedImages));
  }

  onDivScroll($event: Event): void {
    const currentScroll = ($event.target as Element).scrollTop;
    if (this.scrollService.browseScrollY < currentScroll) {
      this.isUserScrollingDown.emit(currentScroll);
    }
    this.scrollService.browseScrollY = currentScroll;
    if (this.scrollService.browseScrollY > 100 && !this.displayScrollToTop) {
      this.displayScrollToTop = true;
      setTimeout(() => this.displayScrollToTop = false, 3000);
    }
  }

}
