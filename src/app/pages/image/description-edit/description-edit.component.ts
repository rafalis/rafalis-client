import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {UntypedFormBuilder, UntypedFormGroup, Validators} from '@angular/forms';
import {ImageDescription, ImageDescriptionUpdate} from '../../../common/models/rafalis-model';
import {AuthService} from '../../../services/auth.service';
import {DatePipe} from '@angular/common';
import {MatChipInputEvent} from '@angular/material/chips';
import {Tag} from '../../add-image/add-image.component';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {MobileService} from '../../../services/mobile.service';
import {MatButtonToggleChange} from '@angular/material/button-toggle';
import {AppConfigService} from '../../../services/app-config.service';
import {MatSlideToggleChange} from '@angular/material/slide-toggle';

@Component({
  selector: 'app-description-edit',
  templateUrl: './description-edit.component.html',
  styleUrls: ['./description-edit.component.scss']
})
export class DescriptionEditComponent implements OnInit {

  @Input() currentDescription: ImageDescription;
  @Output() newDescriptionToUpdate = new EventEmitter<ImageDescriptionUpdate>();
  @Output() deleteRequested = new EventEmitter<void>();
  deleteConfirm: boolean;
  descriptionForm: UntypedFormGroup;
  updating = false;
  addOnBlur = true;
  dateUnknown = false;
  tags: Tag[] = [];
  readonly separatorKeysCodes = [ENTER, COMMA] as const;

  constructor(private formBuilder: UntypedFormBuilder,
              readonly authService: AuthService,
              private datepipe: DatePipe,
              readonly mobileService: MobileService,
              readonly userService: AppConfigService) {
    this.descriptionForm = this.formBuilder.group({
      title: ['', Validators.required],
      description: [''],
      datetime: ['', Validators.required],
      author: ['', Validators.required],
      camera: ['', Validators.required],
      tags: [''],
      ispublic: [''],
      hide_in_browse: [''],
      show_in_home: ['']
    });
  }

  ngOnInit(): void {
    this.descriptionForm.patchValue({
      title: this.currentDescription.title,
      description: this.currentDescription.description,
      datetime: new Date(this.currentDescription.datetime?.split('-').reverse().join('-')),
      author: this.currentDescription.author.split(','),
      camera: this.currentDescription.camera,
      tags: this.currentDescription.tags,
      ispublic: this.currentDescription.ispublic,
      hide_in_browse: !this.currentDescription.show_in_browse,
      show_in_home: this.currentDescription.show_in_home
    });
    if (this.currentDescription.datetime === null) {
      this.toggleDate();
    }
    if (this.currentDescription.tags.length > 0) {
      this.currentDescription.tags.split(',').forEach(value => this.tags.push({name: value}));
    }
  }

  add(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();
    if (value) {
      this.tags.push({name: value});
      this.descriptionForm.patchValue({tags: this.tags.map(v => v.name).join(',')});
    }
    event.input.value = '';
  }

  remove(tag: Tag): void {
    const index = this.tags.indexOf(tag);
    if (index >= 0) {
      this.tags.splice(index, 1);
      this.descriptionForm.patchValue({tags: this.tags.map(v => v.name).join(',')});
    }
  }

  private parseDate(date: Date): string {
    try {
      return this.datepipe.transform(date, 'dd-MM-YYYY');
    } catch (e: any) {
      return null;
    }
  }

  private getFormValue(): ImageDescriptionUpdate {
    return {
      title: this.descriptionForm.value.title,
      description: this.descriptionForm.value.description,
      datetime: this.parseDate(this.descriptionForm.value.datetime),
      author: this.descriptionForm.value.author.join(','),
      camera: this.descriptionForm.value.camera,
      tags: this.descriptionForm.value.tags,
      ispublic: this.descriptionForm.value.ispublic,
      show_in_browse: !this.descriptionForm.value.hide_in_browse,
      show_in_home: this.descriptionForm.value.show_in_home
    };
  }

  onUpdateClick(): void {
    if (this.descriptionForm.value.title !== this.currentDescription.title ||
      this.descriptionForm.value.description !== this.currentDescription.description ||
      this.descriptionForm.value.datetime !== this.currentDescription.datetime ||
      this.descriptionForm.value.author !== this.currentDescription.author ||
      this.descriptionForm.value.camera !== this.currentDescription.camera ||
      this.descriptionForm.value.tags !== this.currentDescription.tags) {
      this.updating = true;
      this.newDescriptionToUpdate.emit(this.getFormValue());
      setTimeout(() => this.updating = false, 1000);
    } else {
      this.newDescriptionToUpdate.emit(null);
    }

  }

  deleteImage(): void {
    this.deleteConfirm = false;
    this.deleteRequested.emit();
  }

  cancel(): void {
    this.newDescriptionToUpdate.emit(null);
  }

  togglePublic($event: MatButtonToggleChange): void {
    this.descriptionForm.patchValue({ispublic: $event.value});
  }

  toggleDate(): void {
    this.dateUnknown = !this.dateUnknown;
    this.dateUnknown ? this.descriptionForm.get('datetime').disable() : this.descriptionForm.get('datetime').enable();
  }

  onHomePageChange($event: MatSlideToggleChange): void {
    if ($event.checked && !this.descriptionForm.value.ispublic) {
      this.descriptionForm.patchValue({ispublic: true});
    }
  }
}
