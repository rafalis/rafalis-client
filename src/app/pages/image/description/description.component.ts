import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {CollectionSummary, ImageDescription} from '../../../common/models/rafalis-model';
import {AuthService} from '../../../services/auth.service';
import {CollectionService} from '../../../services/collection.service';
import {Router} from '@angular/router';
import {MatDialog} from '@angular/material/dialog';
import {MapPlacerDialogComponent, MapPlacerDialogConfig} from '../../../common/map-placer-dialog/map-placer-dialog.component';
import {MobileService} from '../../../services/mobile.service';

@Component({
  selector: 'app-description',
  templateUrl: './description.component.html',
  styleUrls: ['./description.component.scss']
})
export class DescriptionComponent implements OnInit, OnChanges {

  @Input() currentDescription: ImageDescription;
  @Output() descriptionEditRequested: EventEmitter<void> = new EventEmitter<void>();
  @Output() downloadRequested: EventEmitter<void> = new EventEmitter<void>();
  collectionsSummaries: CollectionSummary[] = [];
  mobile = false;

  constructor(readonly authService: AuthService,
              private readonly collectionService: CollectionService,
              private readonly router: Router,
              private readonly dialog: MatDialog,
              private readonly mobileService: MobileService) {
    this.mobile = this.mobileService.isMobileScreen;
  }

  async ngOnChanges(changes: SimpleChanges): Promise<void> {
    if (changes.currentDescription?.isFirstChange()) {
      return;
    }
    await this.retrieveAssociatedCollections();
  }

  async ngOnInit(): Promise<void> {
    await this.retrieveAssociatedCollections();
  }

  private async retrieveAssociatedCollections(): Promise<void> {
    this.collectionsSummaries = [];
    for (const colid of this.currentDescription.collections_appearance) {
      try {
        const collection = await this.collectionService.getCollection(colid);
        this.collectionsSummaries.push(collection);
      } catch (e) {
        if (e.code !== 403) {
          throw e;
        }
      }
    }
  }

  openCollection(collectionID: number): void {
    const url = this.router.serializeUrl(this.router.createUrlTree([`/collections/${collectionID}`], {}));
    window.open(url, '_blank');
  }

  openMapPinner(): void {
    const config: MapPlacerDialogConfig = {
      imageDescription: this.currentDescription,
      mapID: this.currentDescription.map_appearance ? this.currentDescription.map_appearance : -1,
      openedFromAdmin: false,
      placeMe: !this.currentDescription.map_appearance
    };
    const dialog = this.dialog.open(MapPlacerDialogComponent, {
      data: config,
      width: this.mobileService.isMobileScreen ? '100vw' : '90vw'
    });
    dialog.componentInstance.changesSaved.subscribe(() => {
      dialog.close();
    });
  }
}
