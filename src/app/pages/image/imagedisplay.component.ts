import {AfterViewInit, Component, HostListener, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {IMAGE_QUALITY, ImageDescription, ImageDescriptionUpdate} from '../../common/models/rafalis-model';
import {ImageService} from '../../services/image.service';
import {RafalisWsError} from '../../services/picportosws/rafalis-ws.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {FullImageComponent} from './full-image/full-image.component';
import {MobileService} from '../../services/mobile.service';
import {AuthService} from '../../services/auth.service';
import {HammerGestureConfig, Title} from '@angular/platform-browser';
import {ColorService} from '../../services/color.service';
import {fromEvent} from 'rxjs';
import {takeWhile} from 'rxjs/operators';

interface PaletteStyle {
  'background-color': '';
}

@Component({
  selector: 'app-imagedisplay',
  templateUrl: './imagedisplay.component.html',
  styleUrls: ['./imagedisplay.component.scss']
})
export class ImagedisplayComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild('fullImage') imageOverlayChild: FullImageComponent;
  image: ImageDescription;
  displayImage = true;
  transitioning = false;
  nextID = -1;
  previousID = -1;
  displayDescriptionForm = false;
  paletteStyles: PaletteStyle[] = [];
  mobileFlag: boolean;
  matcardStyle: any;
  backgroundStyle: any;
  imageID: number;
  private alive: boolean = true;
  private hammer: any;

  @HostListener('contextmenu', ['$event'])
  onRightClick(event): void {
    event.preventDefault();
  }

  @HostListener('window:keyup', ['$event'])
  async keyEvent(event: KeyboardEvent): Promise<void> {
    if (this.displayDescriptionForm) {
      return;
    }
    if (event.key === 'ArrowLeft') {
      await this.goPrevious();
    } else if (event.key === 'ArrowRight') {
      await this.goNext();
    }
  }

  constructor(private activatedRoute: ActivatedRoute,
              private router: Router,
              private imageService: ImageService,
              private snackbar: MatSnackBar,
              readonly mobileService: MobileService,
              readonly auth: AuthService,
              private readonly colorService: ColorService,
              private titleService: Title) {
  }

  ngOnDestroy(): void {
    this.alive = false;
  }

  async ngOnInit(): Promise<void> {
    this.mobileFlag = this.mobileService.isMobileScreen;
    /* for a reason this.init() is called when subscribing to the paramMap. So it is called in ngAferViewInit() */
  }

  @HostListener('window:resize')
  onResize(): void {
    this.mobileFlag = innerWidth < this.mobileService.MOBILE_WIDTH_THRESHOLD;
  }

  ngAfterViewInit(): void {
    this.activatedRoute.queryParamMap.subscribe(async (_) => {
      this.displayDescriptionForm = false;
      await this.init();
      if (!this.hammer) {
        const hammerConfig = new HammerGestureConfig();
        this.hammer = hammerConfig.buildHammer(document.getElementById('image-container'));
        fromEvent(this.hammer, 'swipe').pipe(
          takeWhile(() => this.alive))
          .subscribe((res: any) => {
            const xDirection = Math.abs(res.deltaX) > 40 ? (res.deltaX > 0 ? 'right' : 'left') : '';
            if (!xDirection) {
              return;
            }
            if (xDirection === 'left') {
              this.goNext();
            } else {
              this.goPrevious();
            }
          });
      }
    });
  }

  async init(): Promise<void> {
    const id = +this.activatedRoute.snapshot.queryParamMap.get('id');

    try {
      this.image = await this.imageService.getImageDescription(id);
      this.imageID = this.image.id;
      // @ts-ignore
    } catch (e: RafalisWsError) {
      if (e.code === 403) {
        this.auth.loginBeforeAccessingContent();
      }
      this.displayImage = false;
      return;
    }
    const lightened = this.colorService.lightenPalette(this.image.palette);
    this.matcardStyle = {
      'background': `rgba(0, 0, 0, 0) linear-gradient(to right bottom,
        ${lightened[0]} 0%,
        ${lightened[1]} 100%
      ) no-repeat scroll 100% center`
    };
    this.backgroundStyle = {
      'background': `rgba(0, 0, 0, 0) linear-gradient(to right bottom,
        ${lightened[4]} 0%,
        ${lightened[3]} 100%
      ) no-repeat scroll 100% center`
    };

    this.paletteStyles = [];
    this.image.palette.forEach((v: string) =>
      this.paletteStyles.push({'background-color': v} as PaletteStyle));

    const nextIndex = this.imageService.idsToDisplayNoHidden.indexOf(this.image.id);
    this.nextID = (nextIndex >= 0 && nextIndex < this.imageService.idsToDisplayNoHidden.length - 1) ?
      this.imageService.idsToDisplayNoHidden[nextIndex + 1] : -1;
    const prevIndex = this.imageService.idsToDisplayNoHidden.indexOf(this.image.id);
    this.previousID = (prevIndex > 0 && prevIndex < this.imageService.idsToDisplayNoHidden.length) ?
      this.imageService.idsToDisplayNoHidden[prevIndex - 1] : -1;

    this.transitioning = false;
    this.titleService.setTitle('Rafalis - ' + this.image.title);
  }

  toggleDescriptionForm(): void {
    this.displayDescriptionForm = !this.displayDescriptionForm;
  }

  async updateDescription(newDes: ImageDescriptionUpdate): Promise<void> {
    if (newDes === null) {
      this.toggleDescriptionForm();
      return;
    }
    try {
      const res = await this.imageService.updateDescription(this.image.id, newDes);
      if (res.message === 'success') {
        this.snackbar.open('Fields successfully updated !', '', {duration: 2000});
      } else {
        this.snackbar.open('Failed to update fields, please try again.', '', {duration: 4000, verticalPosition: 'top'});
        return;
      }
    } catch (e) {
      this.snackbar.open('Failed to update fields, please try again.', '', {duration: 4000, verticalPosition: 'top'});
      return;
    }
    this.toggleDescriptionForm();
    await this.init();
  }

  async deleteImage(): Promise<void> {
    try {
      await this.imageService.deleteImage(this.image.id);
      this.displayImage = false;
      const snack = this.snackbar.open('Image successfully deleted', 'Go back', {duration: 5000});
      snack.onAction().subscribe(() => this.router.navigateByUrl('/browse'));
    } catch (error) {
      this.snackbar.open(error.message, 'Okay');
    }
  }

  async goPrevious(): Promise<void> {
    if (this.previousID === -1) {
      return;
    }
    this.imageID = null;
    this.transitioning = true;
    await this.router.navigateByUrl(`/img?id=${this.previousID}`);
  }

  async goNext(): Promise<void> {
    if (this.nextID === -1) {
      return;
    }
    this.imageID = null;
    this.transitioning = true;
    await this.router.navigateByUrl(`/img?id=${this.nextID}`);
  }

  async onImageClicked(): Promise<void> {
    this.imageOverlayChild.imageId = this.imageID;
    this.imageOverlayChild.enableButton = false;
    await this.imageOverlayChild.onChange();
    this.imageOverlayChild.show = true;
  }

  async downloadImage(): Promise<void> {
    if (this.auth.isAdmin) {
      const a = document.createElement('a');
      const response = await this.imageService.getImageBytes(this.image.id, IMAGE_QUALITY.FULL_QUALITY);
      a.href = URL.createObjectURL(response.body);
      a.download = this.image.name.replace(' ', '_');
      a.click();
    }
  }
}
