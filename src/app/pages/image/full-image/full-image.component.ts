import {Component, HostListener} from '@angular/core';
import {Router} from '@angular/router';
import {MobileService} from '../../../services/mobile.service';
import {AuthService} from "../../../services/auth.service";
import {ImageService} from "../../../services/image.service";
import {RafalisWsError} from "../../../services/picportosws/rafalis-ws.service";
import {IMAGE_QUALITY} from "../../../common/models/rafalis-model";

@Component({
  selector: 'app-full-image',
  templateUrl: './full-image.component.html',
  styleUrls: ['./full-image.component.scss']
})
export class FullImageComponent {

  imageURL: any;
  imageId: number;
  imageTitle: string = "";
  show: boolean = false;
  enableButton: boolean = true;
  mobile: boolean;
  nextID: number = -1;
  previousID: number = -1;

  @HostListener('contextmenu', ['$event'])
  onRightClick($event): void {
    $event.preventDefault();
  }

  @HostListener('window:keyup', ['$event'])
  async keyEvent(event: KeyboardEvent): Promise<void> {
    if (!this.show) {
      return;
    }
    if (event.key === 'ArrowLeft') {
      await this.goPrevious();
    } else if (event.key === 'ArrowRight') {
      await this.goNext();
    }
  }

  constructor(private router: Router,
              private mobileService: MobileService,
              private readonly imageService: ImageService,
              private readonly authService: AuthService) {
    this.mobile = this.mobileService.isMobileScreen;
  }

  async onOpenImageClicked(): Promise<void> {
    this.show = false;
    await this.router.navigateByUrl(`img?id=${this.imageId}`);
  }

  onClose(): void {
    this.show = false;
    URL.revokeObjectURL(this.imageURL);
  }

  async onChange(): Promise<void> {
    try {
      const imageDescription = await this.imageService.getImageDescription(this.imageId);
      this.imageTitle = this.imageService.getOverlayImageTitle(imageDescription);
      if (this.authService.authenticated) {
        const imageBlob = await this.imageService.getImageBytes(this.imageId, IMAGE_QUALITY.SMALL_QUALITY);
        this.imageURL = this.imageService.createImageFromBlob(imageBlob.body);
      } else {
        this.imageURL = this.imageService.getImageURL(this.imageId, IMAGE_QUALITY.SMALL_QUALITY);
      }
      // @ts-ignore
    } catch (e: RafalisWsError) {
      console.log('Error for image #' + this.imageId);
      return;
    }
    const nextIndex = this.imageService.idsToDisplayNoHidden.indexOf(this.imageId);
    this.nextID = (nextIndex >= 0 && nextIndex < this.imageService.idsToDisplayNoHidden.length - 1) ?
      this.imageService.idsToDisplayNoHidden[nextIndex + 1] : -1;
    const prevIndex = this.imageService.idsToDisplayNoHidden.indexOf(this.imageId);
    this.previousID = (prevIndex > 0 && prevIndex < this.imageService.idsToDisplayNoHidden.length) ?
      this.imageService.idsToDisplayNoHidden[prevIndex - 1] : -1;
  }

  async goPrevious(): Promise<void> {
    if (this.previousID === -1) {
      return;
    }
    this.imageId = this.previousID;
    await this.onChange();
  }

  async goNext(): Promise<void> {
    if (this.nextID === -1) {
      return;
    }
    this.imageId = this.nextID;
    await this.onChange();
  }

}
