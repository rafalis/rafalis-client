import {ComponentFixture, TestBed} from '@angular/core/testing';

import {MapBrowserComponent} from './map-browser.component';

describe('MapBrowserComponent', () => {
  let component: MapBrowserComponent;
  let fixture: ComponentFixture<MapBrowserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MapBrowserComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MapBrowserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
