import {Component, OnInit} from '@angular/core';
import {MapSummary} from '../../common/models/rafalis-model';
import {MapService} from '../../services/map.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MobileService} from '../../services/mobile.service';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'app-map-browser',
  templateUrl: './map-browser.component.html',
  styleUrls: ['./map-browser.component.scss']
})
export class MapBrowserComponent implements OnInit {

  maps: MapSummary[];
  currentSelectedMap: MapSummary;
  mapsIDToDelete = new Set<number>();
  mapIsLoading = false;
  mobile: boolean;

  constructor(readonly mapService: MapService,
              private snackbar: MatSnackBar,
              private mobileService: MobileService,
              private titleService: Title) {
    this.titleService.setTitle('Rafalis - Maps');
  }

  async ngOnInit(): Promise<void> {
    this.mobile = this.mobileService.isMobileScreen;
    this.mapIsLoading = true;
    this.maps = await this.mapService.getMapSummaries();
    this.maps = this.mapService.orderMaps(this.maps);
  }

  onMapSelectionChange(): void {
    this.mapIsLoading = true;
    setTimeout(() => {
      if (this.mapIsLoading) {
        this.snackbar.open('Failed to load map, please try again', '', {duration: 5000, verticalPosition: 'top'});
      }
      this.mapIsLoading = false;
    }, 10000);
  }

  onMapIsLoaded(): void {
    this.mapIsLoading = false;
    if (this.maps[0] && !this.currentSelectedMap) {
      this.currentSelectedMap = this.maps[0];
    }
  }
}
