import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './pages/login/login.component';
import {HomeComponent} from './pages/home/home.component';
import {ImagedisplayComponent} from './pages/image/imagedisplay.component';
import {CollectionsComponent} from './pages/collections/collections.component';
import {AboutComponent} from './pages/about/about.component';
import {CollectionDisplayComponent} from './pages/collections/collection-display/collection-display.component';
import {AdminComponent} from './pages/admin/admin.component';
import {AuthGuard} from './services/auth.guard';
import {MapBrowserComponent} from './pages/map-browser/map-browser.component';
import {MapPlacerDialogComponent} from './common/map-placer-dialog/map-placer-dialog.component';


const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'img', component: ImagedisplayComponent},
  {path: 'collections', component: CollectionsComponent},
  {path: 'collections/:coll-id', component: CollectionDisplayComponent},
  {path: 'maps', component: MapBrowserComponent},
  {path: 'about', component: AboutComponent},
  {path: 'maps/placer', component: MapPlacerDialogComponent, canActivate: [AuthGuard]},
  {path: 'admin', component: AdminComponent, canActivate: [AuthGuard]},
  {path: '**', redirectTo: '', pathMatch: 'full'}
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes, {})
  ]
})
export class AppRoutingModule { }
