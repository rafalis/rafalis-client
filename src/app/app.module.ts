import {APP_INITIALIZER, NgModule} from '@angular/core';
import {BrowserModule, HAMMER_GESTURE_CONFIG, HammerGestureConfig} from '@angular/platform-browser';
import * as Hammer from 'hammerjs';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {LoginComponent} from './pages/login/login.component';
import {RouterModule} from '@angular/router';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatCardModule} from '@angular/material/card';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {HomeComponent} from './pages/home/home.component';
import {appInitializer} from './app-initializer';
import {ConfigService} from './services/config.service';
import {MatMenuModule} from '@angular/material/menu';
import {FiltersComponent} from './common/filters/filters.component';
import {MatSelectModule} from '@angular/material/select';
import {MAT_DATE_LOCALE, MatNativeDateModule, MatOptionModule, MatRippleModule} from '@angular/material/core';
import {MatIconModule} from '@angular/material/icon';
import {ThumListComponent} from './pages/browse/thum-list/thum-list.component';
import {ImageThumComponent} from './pages/browse/image-thum/image-thum.component';
import {MatDialogModule} from '@angular/material/dialog';
import {ImagedisplayComponent} from './pages/image/imagedisplay.component';
import {AuthInterceptor} from './auth.interceptor';
import {DescriptionComponent} from './pages/image/description/description.component';
import {DescriptionEditComponent} from './pages/image/description-edit/description-edit.component';
import {NavBarComponent} from './common/nav-bar/nav-bar.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {CollectionsComponent} from './pages/collections/collections.component';
import {AddImageComponent} from './pages/add-image/add-image.component';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {FullImageComponent} from './pages/image/full-image/full-image.component';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {DatePipe, NgOptimizedImage} from '@angular/common';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatChipsModule} from '@angular/material/chips';
import {AboutComponent} from './pages/about/about.component';
import {MatGridListModule} from '@angular/material/grid-list';
import {CollectionDisplayComponent} from './pages/collections/collection-display/collection-display.component';
import {TextContentComponent} from './pages/collections/collection-display/text-content/text-content.component';
import {ImageContentComponent} from './pages/collections/collection-display/image-content/image-content.component';
import {AngularEditorModule} from '@kolkov/angular-editor';
import {AddCollectionComponent} from './pages/add-collection/add-collection.component';
import {LAZYLOAD_IMAGE_HOOKS, LazyLoadImageModule} from 'ng-lazyload-image';
import {LazyLoadImageHooks} from './services/lazy-hook';
import {MapComponent} from './common/map/map.component';
import {MapConfiguratorComponent} from './common/map/map-configurator/map-configurator.component';
import {ImageComponent} from './common/image/image.component';
import {AdminComponent} from './pages/admin/admin.component';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {MapBrowserComponent} from './pages/map-browser/map-browser.component';
import {MatExpansionModule} from '@angular/material/expansion';
import {GuidesComponent} from './common/guides/guides.component';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {
  CarouselContentComponent
} from './pages/collections/collection-display/carousel-content/carousel-content.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ModifierComponent} from './pages/collections/modifier/modifier.component';
import {ServiceWorkerModule} from '@angular/service-worker';
import {environment} from '../environments/environment';
import {RotatorComponent} from './pages/admin/rotator/rotator.component';
import {SecurePipe} from './pipes/secure.pipe';
import {AdminconfigComponent} from './pages/admin/adminconfig/adminconfig.component';
import {MatRadioModule} from '@angular/material/radio';
import {MapPlacerDialogComponent} from './common/map-placer-dialog/map-placer-dialog.component';
import {MatSliderModule} from '@angular/material/slider';
import {ToHumanDistancePipe} from './pipes/to-human-distance.pipe';
import {AdminMapComponent} from './pages/admin/admin-map/admin-map.component';
import {DisplayListProperlyPipe} from './pipes/display-list-properly.pipe';
import {NgxMasonryModule} from "ngx-masonry";

export class MyHammerConfig extends HammerGestureConfig {
  overrides = <any>{
    swipe: {direction: Hammer.DIRECTION_HORIZONTAL},
    pinch: {enable: false},
    rotate: {enable: false}
  };
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    FiltersComponent,
    ThumListComponent,
    ImageThumComponent,
    ImagedisplayComponent,
    DescriptionComponent,
    DescriptionEditComponent,
    NavBarComponent,
    CollectionsComponent,
    AddImageComponent,
    FullImageComponent,
    AboutComponent,
    CollectionDisplayComponent,
    TextContentComponent,
    ImageContentComponent,
    AddCollectionComponent,
    MapComponent,
    MapConfiguratorComponent,
    ImageComponent,
    AdminComponent,
    MapBrowserComponent,
    GuidesComponent,
    CarouselContentComponent,
    ModifierComponent,
    RotatorComponent,
    SecurePipe,
    AdminconfigComponent,
    MapPlacerDialogComponent,
    ToHumanDistancePipe,
    AdminMapComponent,
    DisplayListProperlyPipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatCardModule,
    MatButtonModule,
    MatSnackBarModule,
    MatInputModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatMenuModule,
    MatDialogModule,
    MatSelectModule,
    MatOptionModule,
    MatIconModule,
    MatRadioModule,
    FormsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatProgressBarModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSlideToggleModule,
    MatChipsModule,
    MatGridListModule,
    AngularEditorModule,
    LazyLoadImageModule,
    MatRippleModule,
    MatTableModule,
    MatSortModule,
    MatExpansionModule,
    MatProgressSpinnerModule,
    NgbModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
      // Register the ServiceWorker as soon as the app is stable
      // or after 30 seconds (whichever comes first).
      registrationStrategy: 'registerWhenStable:30000'
    }),
    MatSliderModule,
    NgxMasonryModule,
    NgOptimizedImage
  ],
  providers: [
    {provide: MAT_DATE_LOCALE, useValue: 'fr-FR'},
    {
      provide: APP_INITIALIZER,
      useFactory: appInitializer,
      multi: true,
      deps: [ConfigService, HttpClient]
    },
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
    {provide: LAZYLOAD_IMAGE_HOOKS, useClass: LazyLoadImageHooks},
    {provide: HAMMER_GESTURE_CONFIG, useClass: MyHammerConfig},
    DatePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
