import {Component, EventEmitter, Inject, OnInit, Optional, ViewChild} from '@angular/core';
import * as mapboxgl from 'mapbox-gl';
import {GeoJSONSource} from 'mapbox-gl';
import {MapService} from '../../services/map.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatSlider, MatSliderChange} from '@angular/material/slider';
import {MatSlideToggle, MatSlideToggleChange} from '@angular/material/slide-toggle';
import {ImageService} from '../../services/image.service';
import {ImageDescription, MapSummary} from '../models/rafalis-model';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {MatSelect} from '@angular/material/select';
import circle from '@turf/circle';

export interface RafalisFeature {
  type: string,
  properties: {
    name: string,
    range: boolean,
    description?: string,
    rangeSize?: number,
  },
  geometry: {
    coordinates: number[]
    type: string
  }
}

export interface MapPlacerDialogConfig {
  imageDescription: ImageDescription,
  mapID?: number,
  openedFromAdmin?: boolean,
  placeMe?: boolean
}

@Component({
  selector: 'app-map-placer-dialog',
  templateUrl: './map-placer-dialog.component.html',
  styleUrls: ['./map-placer-dialog.component.scss']
})
export class MapPlacerDialogComponent implements OnInit {

  @ViewChild('rangeToggle') rangeToggle: MatSlideToggle;
  @ViewChild('rangeSlider') rangeSlider: MatSlider;
  @ViewChild('mapSelector') mapSelector: MatSelect;

  private map: mapboxgl.Map;
  private mapboxToken = 'pk.eyJ1IjoicmFmYWxpcyIsImEiOiJja3dwcmI1bGwwZmc2MnBtOHNmYzBnajRsIn0.NoYqQB5eljPPa3PpRejVnw';
  private mapboxStyle = 'mapbox://styles/mapbox/satellite-streets-v11';

  private geojson: any;

  private mapID = -1;
  private imageID = null;

  mapSummaries: MapSummary[] = [];
  currentMap: MapSummary;
  rangeSize: number = 100;
  rangeSliderValue: number = 0;
  mapIsLoading = false;
  openFromAdmin = false;

  changesSaved = new EventEmitter<void>();

  currentFeature: RafalisFeature;
  currentCircle: any;
  currentMarker: mapboxgl.Marker;
  currentPopup: mapboxgl.Popup;

  isPlacingMarker = false;

  plottedMarkers: mapboxgl.Marker[] = [];
  plottedCircles = [];

  private circleLayerId = 'circlesLayers';
  private circleSourceId = 'circlesSource';

  imageDescription: ImageDescription;

  constructor(readonly mapService: MapService,
              readonly imageService: ImageService,
              private snackbar: MatSnackBar,
              @Optional() @Inject(MAT_DIALOG_DATA) private dialogData: MapPlacerDialogConfig) {
    this.imageDescription = dialogData.imageDescription;
    this.imageID = dialogData.imageDescription?.id;
    this.openFromAdmin = dialogData.openedFromAdmin;
    this.isPlacingMarker = dialogData.placeMe;
  }

  ngOnInit(): void {
    this.mapID = this.dialogData.mapID;
    this.mapService.getMapSummaries().then(async res => {
        // Place the Rafalis base map first
        this.mapSummaries = this.mapService.orderMaps(res);
        if (this.mapID === -1) {
          // Rafalis base map according to the sort above ^
          this.currentMap = this.mapSummaries[0];
          this.mapID = this.currentMap.id;
        } else {
          this.currentMap = res.filter(m => m.id === this.mapID)[0];
        }
      },
      err => err
    );
    this.map = new mapboxgl.Map({
      accessToken: this.mapboxToken,
      container: 'map-pin',
      style: this.mapboxStyle,
      center: [8.507494, 46.607913],
      zoom: 5.8
    });
    this.map.addControl(new mapboxgl.NavigationControl())
      .addControl(new mapboxgl.FullscreenControl());
    this.fetchGeoJson();
    this.initMap();
  }

  private fetchGeoJson(): void {
    if (this.mapID === null) {
      return;
    }
    this.mapService.getGeoJson(this.mapID)
      .then(response => {
        response.body.text().then(object => {
          this.geojson = JSON.parse(object);
          if (this.map?.loaded()) {
            this.plotCurrentMap();
          }
        });
      })
      .catch(err => {
        if (err.code === 404) {
          this.snackbar.open('Map #' + this.mapID + ' not found.', '', {duration: 5000, verticalPosition: 'top'});
        } else {
          this.snackbar.open('Error while retrieving the map.', '', {duration: 5000, verticalPosition: 'top'});
        }
      });
  }

  private initMap(): void {
    this.map.on('load', () => {
      if (document.getElementById('map-pin')?.children.length === 0) {
        console.log('Map reload triggered');
        this.ngOnInit();
        return;
      }
      if (!!this.geojson) {
        this.plotCurrentMap();
      }
      this.mapIsLoading = false;
    });
  }

  private fitTheBounds(): void {
    if (this.geojson.features.length === 0) {
      return;
    }
    const bounds = new mapboxgl.LngLatBounds();
    this.geojson.features.forEach(f => bounds.extend(f.geometry.coordinates));
    this.map.fitBounds(bounds, {padding: 20, maxZoom: 8, duration: 800});
  }

  private addNewMarker(): void {
    if (this.currentMarker) {
      return;
    }
    const newMarker = new mapboxgl.Marker({color: '#f22'})
      .setLngLat(this.map.getCenter())
      .addTo(this.map);
    const onMouseMove = (ev): void => {
      newMarker.setLngLat(ev.lngLat);
    };
    this.map.on('mousemove', onMouseMove);
    this.map.once('click', (e) => {
      const newFeature: RafalisFeature = {
        type: 'Feature',
        properties: {
          name: this.imageDescription?.name ? this.imageDescription.name : 'Rafalis Image',
          description: `!img${this.imageDescription.id}`,
          range: false,
          rangeSize: 0,
        },
        geometry: {
          type: 'Point',
          coordinates: e.lngLat.toArray()
        }
      };
      this.geojson.features.push(newFeature);
      newMarker.remove();
      this.map.off('mousemove', onMouseMove);
      this.plotFeatureAsMarker(newFeature);
      this.plottedMarkers[this.plottedMarkers.length - 1].getElement().click();
      this.isPlacingMarker = false;
    });
    this.currentMarker = newMarker;
  }

  private plotCurrentMap(): void {
    this.geojson.features.filter(f => {
      return f?.geometry?.type === 'Point' && f?.properties?.description?.includes(`!img`);
    })
      .forEach(f => this.plotFeatureAsMarker(f));

    if (this.isPlacingMarker) {
      this.addNewMarker();
      this.fitTheBounds();
    } else {
      this.fitTheBounds();
    }

    this.map.addSource(this.circleSourceId, {
      type: 'geojson',
      data: {type: 'FeatureCollection', features: this.plottedCircles}
    });

    this.map.addLayer({
      id: this.circleLayerId,
      type: 'fill',
      source: this.circleSourceId,
      paint: {
        'fill-opacity': 0.3,
        'fill-color': 'rgba(0,33,74,0.75)',
      }
    });
  }

  private createNewCircle(id: string, coords: number[], radius: number): any {
    return circle(coords, radius, {units: 'kilometers', steps: 100, properties: {id}});
  };

  private updateCircle(id: string, coords: number[], radius: number): void {
    this.plottedCircles = this.plottedCircles.filter(c => c.properties.id !== id);
    this.plottedCircles.push(this.createNewCircle(id, coords, radius));
    this.updateCircleLayers();
  };

  private getCircle(id: string): any {
    return this.plottedCircles.filter(c => c.properties.id === id);
  };

  private updateCircleLayers(): void {
    if (this.map.getLayer(this.circleLayerId)) {
      this.map.removeLayer(this.circleLayerId);
    }
    (this.map.getSource(this.circleSourceId) as GeoJSONSource).setData({type: 'FeatureCollection', features: this.plottedCircles});
    this.map.addLayer({
      id: this.circleLayerId,
      type: 'fill',
      source: this.circleSourceId,
      paint: {
        'fill-opacity': 0.3,
        'fill-color': 'rgba(0,33,74,0.75)',
      }
    });
  }

  private plotFeatureAsMarker(feature: RafalisFeature): void {
    const marker = new mapboxgl.Marker({color: '#00214A'})
      .setLngLat({lng: feature.geometry.coordinates[0], lat: feature.geometry.coordinates[1]})
      .setDraggable(false)
      .addTo(this.map);
    const rangeSize = feature.properties.range ? feature.properties.rangeSize : 0;

    const featureCoords = [feature.geometry.coordinates[0], feature.geometry.coordinates[1]];

    const c = circle(featureCoords, rangeSize / 1000,
      {units: 'kilometers', steps: 100, properties: {id: feature.properties.description}});

    marker.on('dragstart', () => {
      if (!this.currentFeature) {
        return;
      }
      this.updateCircle(feature.properties.description, featureCoords, 0);
      this.currentPopup.getElement().style.visibility = 'hidden';
    });
    marker.on('dragend', () => {
      if (!this.currentFeature) {
        return;
      }
      this.geojson.features = this.geojson.features.filter(f => f.properties.name !== feature.properties.name);
      const newCoords = feature;
      feature.geometry.coordinates = marker.getLngLat().toArray();
      this.geojson.features.push(newCoords);
      const coords = marker.getLngLat();
      const newRangeSize = this.currentFeature.properties.range ? this.currentFeature.properties.rangeSize / 1000 : 0;

      this.updateCircle(feature.properties.description, [coords.lng, coords.lat], newRangeSize);
      this.currentPopup.setLngLat(coords);
      this.currentPopup.getElement().style.visibility = 'visible';
    });
    marker.getElement().addEventListener('click', () => {
      const select = (): void => {
        if (this.currentPopup) {
          this.currentPopup?.remove();
          this.currentPopup = null;
        }
        this.currentFeature = feature;

        this.currentCircle = this.getCircle(feature.properties.description);
        marker.setDraggable(true);
        this.currentPopup = new mapboxgl.Popup({closeButton: false, closeOnClick: false, offset: 40, className: 'hide-popup-content'})
          .setHTML(`<h2> </h2>`) // css will hide this popup content
          .setLngLat(marker.getLngLat())
          .addTo(this.map);
        this.currentMarker = marker;
        this.rangeSliderValue = rangeSize;
        this.rangeToggle.checked = feature.properties.range;
      };
      if (!this.currentFeature) {
        select();
      } else {
        if (feature !== this.currentFeature) {
          select();
        } else {
          this.deselectMarker();
        }
      }
    });
    if (!this.openFromAdmin && !this.isPlacingMarker) {
      if (feature.properties?.description?.includes(`!img${this.imageID}`)) {
        marker.getElement().click();
      }
    }
    this.plottedMarkers.push(marker);
    this.plottedCircles.push(c);
  }

  private deselectMarker(): void {
    this.currentPopup.remove();
    this.currentPopup = null;
    this.currentMarker.setDraggable(false);
    this.currentMarker = null;
    this.currentCircle = null;
    this.currentFeature = null;
  }

  onToggleChange(change: MatSlideToggleChange): void {
    this.geojson.features.filter(f => f.properties.name === this.currentFeature.properties.name)
      [0].properties.range = change.checked;
    this.plottedCircles = this.plottedCircles.filter(c => c.properties.id !== this.currentFeature.properties.description);
    if (change.checked) {
      const rangeSize = this.currentFeature.properties?.rangeSize ? this.currentFeature.properties.rangeSize / 1000 : 5;
      const coords = [this.currentFeature.geometry.coordinates[1], this.currentFeature.geometry.coordinates[0]];
      this.plottedCircles.push(this.createNewCircle(this.currentFeature.properties.description, coords, rangeSize));
    }
    this.updateCircleLayers();
  }

  onSliderChange($event: MatSliderChange): void {
    if (!this.currentFeature.properties.range) {
      return;
    }
    const coords = [this.currentFeature.geometry.coordinates[0], this.currentFeature.geometry.coordinates[1]];
    this.updateCircle(this.currentFeature.properties.description, coords, $event.value === 0 ? 100 / 1000 : $event.value / 1000);

    this.geojson.features.filter(f => f.properties.name === this.currentFeature.properties.name)
      [0].properties.rangeSize = $event.value;
  }

  async saveChanges(): Promise<void> {
    const form = new FormData();
    const file = new File([JSON.stringify(this.geojson)], this.currentMap.filename, {
      type: 'application/json',
    });
    form.append('json', file);
    form.append('associatedCollection', null);
    form.append('filename', this.currentMap.filename);
    form.append('name', this.currentMap.name);
    // 0 to not update the collection ID
    await this.mapService.updateMap(form, this.mapID);
    // if there was an image description inputted, we were on a /img page. We can propose to reload the page
    const snack = this.snackbar.open('Changes saved successfully !', this.imageDescription ? 'Reload page to see changes' : '', {duration: 2000});
    snack.onAction().subscribe(() => location.reload());
    this.changesSaved.emit();
  }

  deleteSelected(): void {
    this.geojson.features =
      this.geojson.features.filter(f => f.properties.name !== this.currentFeature.properties.name);
    this.updateCircle(this.currentFeature.properties.description, [this.currentMarker.getLngLat().lng, this.currentMarker.getLngLat().lat], 0);
    this.currentMarker.remove();
    this.currentPopup.remove();
    this.currentMarker = null;
    this.currentCircle = null;
    this.currentPopup = null;
    this.currentFeature = null;
  }

  onMapSelectionChange($event: MapSummary): void {
    if (this.currentFeature) {
      this.deselectMarker();
    }
    if (this.plottedMarkers.length > 0) {
      this.plottedMarkers.forEach(m => m.remove());
      this.plottedMarkers = [];
    }
    if (this.plottedCircles.length > 0) {
      this.plottedCircles = [];
      this.updateCircleLayers();
      this.map.removeLayer(this.circleLayerId);
      this.map.removeSource(this.circleSourceId);
    }
    this.currentMap = $event;
    this.mapID = $event.id;
    this.fetchGeoJson();
  }
}
