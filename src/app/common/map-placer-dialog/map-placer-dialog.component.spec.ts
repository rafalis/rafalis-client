import {ComponentFixture, TestBed} from '@angular/core/testing';

import {MapPlacerDialogComponent} from './map-placer-dialog.component';

describe('MapPlacerDialogComponent', () => {
  let component: MapPlacerDialogComponent;
  let fixture: ComponentFixture<MapPlacerDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MapPlacerDialogComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MapPlacerDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
