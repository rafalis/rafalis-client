export interface ImageItem {
  id: number;
  title: string;
  datetime: string;
  ispublic: boolean;
  show_in_browse: boolean;
  show_in_home: boolean;
  palette: string[];
  tags: string;
  small_width: number;
  small_height: number;
  thumb_width: number;
  thumb_height: number;
  minithumb_width: number;
  minithumb_height: number;
}

export interface ImageDescription {
  id: number;
  name: string;
  title: string;
  description: string;
  datetime: string;
  author: string;
  camera: string;
  tags: string;
  palette: string[];
  collections_appearance: number[];
  ispublic: boolean;
  map_appearance: number;
  show_in_browse: boolean;
  show_in_home: boolean;
}

export interface VersionInfo {
  version: string;
  date: string;
}

export interface ImageDescriptionUpdate {
  title: string;
  description: string;
  author: string;
  datetime: string;
  camera: string;
  tags: string;
  ispublic: boolean;
  show_in_browse: boolean;
  show_in_home: boolean;
}

export interface ImageDescriptionUpdatedStatus {
  message: string;
}

export interface AuthRequest {
  username: string;
  password: string;
}

export interface AuthResponse {
  token: string;
}

export interface ImageAddedConfirmation {
  message: string;
  id: number;
}

export interface Collection {
  id: number;
  name: string;
  description: string;
  thumbnailid: number;
  structure: string;
  textContent: string[];
  imageContent: number[];
  carouselContent: (number[])[];
  ispublic: boolean;
  map_id: number;
  lastModified?: number;
}

export interface CollectionSummary {
  id: number;
  name: string;
  thumbnailid: number;
  description: string;
  ispublic: boolean;
  map_id: number;
}


export interface CollectionUpdateStatus {
  message: string;
}

export interface MapAddedConfirmation {
  id: number;
  message: string;
}

export interface MapSummary {
  id: number;
  name: string;
  filename: string;
  referenced_images: number[];
  referenced_collection: number;
}

export interface MapUpdateStatus {
  message: string;
}

export interface AdminConfig {
  authors: string[];
  cameras: string[];
  mainTitle: string;
  subTitle: string;
}

export interface AdminConfigUpdateStatus {
  message: string;
}

export enum IMAGE_QUALITY {
  FULL_QUALITY = 0,
  SMALL_QUALITY = 1,
  THUMB_QUALITY = 2,
  MINITHUMB_QUALITY = 3
}

export interface RafalisToken {
  sub: string;
  token_type: string;
  account_id: number;
  email: string;
  admin: boolean;
  manager: boolean;
  full_name: string;
  iat: number;
  auth_time: number;
  exp: number;
}

export interface FullImageParams {
  id: number,
  enableButton: boolean
}
