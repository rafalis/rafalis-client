import {Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges} from '@angular/core';
import * as mapboxgl from 'mapbox-gl';
import {MapService} from '../../services/map.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import circle from '@turf/circle';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit, OnChanges, OnDestroy {

  @Input() mapID;
  @Input() enable3D = false;
  @Input() imageIDToFocus = -1;
  @Output() allIsGood = new EventEmitter<void>();

  private map: mapboxgl.Map;
  private mapboxToken = 'pk.eyJ1IjoicmFmYWxpcyIsImEiOiJja3dwcmI1bGwwZmc2MnBtOHNmYzBnajRsIn0.NoYqQB5eljPPa3PpRejVnw';
  private mapboxStyle = 'mapbox://styles/mapbox/satellite-streets-v11';

  private geojson: any;
  private plotSem = false;
  private popupSem = false;

  private layers = new Set<string>();
  private sources = new Set<string>();
  private circles = [];
  private loaded3d = false;
  private imageToFocusPopup: mapboxgl.Popup;

  constructor(private readonly mapService: MapService,
              private snackbar: MatSnackBar) {
  }

  ngOnDestroy(): void {
    if (this.map !== null) {
      this.map.remove();
    }
    this.map = null;
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.mapID?.isFirstChange()) {
      return;
    }
    if (changes.imageIDToFocus?.currentValue !== changes.imageIDToFocus?.previousValue) {
      this.imageIDToFocus = changes.imageIDToFocus?.currentValue;
    }
    this.clearMap();
    this.fetchGeoJson();
    this.initMap();
  }

  private clearMap(): void {
    if (this.layers.size > 0) {
      this.layers.forEach(layerID => this.map.removeLayer(layerID));
      this.layers.clear();
    }
    if (this.sources.size > 0) {
      this.sources.forEach(sourceID => this.map.removeSource(sourceID));
      this.sources.clear();
    }
    if (this.circles.length > 0) {
      this.circles = [];
    }
  }

  async ngOnInit(): Promise<void> {
    this.map = new mapboxgl.Map({
      accessToken: this.mapboxToken,
      container: 'map',
      style: this.mapboxStyle,
      center: [8.507494, 46.607913],
      zoom: 5.8
    });
    this.map.addControl(new mapboxgl.NavigationControl())
      .addControl(new mapboxgl.FullscreenControl());

    this.fetchGeoJson();
    this.initMap();
  }

  private fetchGeoJson(): void {
    if (this.mapID === null) {
      return;
    }
    this.mapService.getGeoJson(this.mapID)
      .then(response => {
        response.body.text().then(object => {
          this.geojson = JSON.parse(object);
          if (this.map?.loaded() && !this.plotSem) {
            this.plot();
          }
        });
      })
      .catch(err => {
        if (err.code === 404) {
          this.snackbar.open('Map #' + this.mapID + ' not found.', '', {duration: 5000, verticalPosition: 'top'});
        } else {
          this.snackbar.open('Error while retrieving the map.', '', {duration: 5000, verticalPosition: 'top'});
        }
      });
  }

  private initMap(): void {
    if (this.map === null) {
      return;
    }
    this.map.on('load', () => {
      // weird, when navigating from a page containing a map to an other page containing a map, the new map will not load,
      // hence this reload trigger
      if (document.getElementById('map').children.length === 0) {
        console.log('Map reload triggered');
        this.ngOnInit();
        return;
      }
      this.allIsGood.emit();
      if (!!this.geojson && !this.plotSem) {
        this.plot();
      }
    });
  }

  private add3D(): void {
    const id3dSource = 'mapbox-dem';
    this.map.addSource(id3dSource, {
      'type': 'raster-dem',
      'url': 'mapbox://mapbox.mapbox-terrain-dem-v1',
      'tileSize': 512,
      'maxzoom': 14
    });
    this.map.setTerrain({'source': id3dSource, 'exaggeration': 1.5});

    const skyLayerID = 'sky';
    this.map.addLayer({
      'id': skyLayerID,
      'type': 'sky',
      'paint': {
        'sky-type': 'atmosphere',
        'sky-atmosphere-sun': [0.0, 0.0],
        'sky-atmosphere-sun-intensity': 15
      }
    });
  }

  private plot(): void {
    if (this.map === null) {
      return;
    }
    this.plotSem = true;
    if (this.enable3D && !this.loaded3d) {
      this.add3D();
      this.loaded3d = true;
    }
    const traces = this.geojson.features.filter(feature => feature.geometry.type === 'GeometryCollection' || feature.geometry.type === 'LineString');
    const points = this.geojson.features.filter(feature => feature.geometry.type === 'Point'
      && feature.properties?.description?.includes('!img'));

    if (traces.length > 0) {
      this.plotTrace(traces);
    }
    if (points.length > 0) {
      this.plotPoints(points);
    }

    const bounds = new mapboxgl.LngLatBounds();
    if (traces.length > 0) {
      traces.forEach(trace => {
        let coordinates = null;
        if (trace.geometry.type === 'GeometryCollection') {
          coordinates = trace.geometry.geometries[0].coordinates;
        } else if (trace.geometry.type === 'LineString') {
          coordinates = trace.geometry.coordinates;
        }
        if (coordinates !== null) {
          for (const coord of coordinates) {
            bounds.extend(coord);
          }
        } else {
          console.log('Unable to parse this json...');
        }
      });
    }

    if (points.length > 0) {
      points.forEach(point => {
        bounds.extend(point.geometry.coordinates);
      });
    }

    this.plotSem = false;
    this.allIsGood.emit();
    if (!bounds.isEmpty()) {
      this.map.fitBounds(bounds, {padding: 30, maxZoom: 13, duration: 800});
    }
    if (this.enable3D && !bounds.isEmpty()) {
      this.map.once('moveend', () => {
        if (this.map.getZoom() < 7) {
          return;
        }
        setTimeout(
          () => this.map.flyTo({
            center: bounds.getCenter(),
            pitch: 45,
            animate: true,
            duration: 500,
            padding: 100,
            essential: true
          }),
          500);
      });
    }

  }

  private plotPoints(points: any): void {

    const pointsSourceID = 'points';
    const pointsLayerID = 'points-layer';

    const circlesSourceID = 'circles';
    const circleLayerID = 'circles-layer';

    this.map.addSource(pointsSourceID, {
      type: 'geojson',
      data: {
        type: 'FeatureCollection',
        features: points
      }
    });
    this.sources.add(pointsSourceID);
    this.map.loadImage('/assets/icon-map-resized.png', (err, image) => {
      if (err) throw err;
      if (!this.map.hasImage('logo')) {
        this.map.addImage('logo', image);
      }
      this.map.addLayer({
        id: pointsLayerID,
        'type': 'symbol',
        'source': pointsSourceID,
        'layout': {
          'icon-image': 'logo',
          'icon-size': 0.25
        }
      });
      this.addHoverEvents(pointsLayerID);
      this.layers.add(pointsLayerID);
    });


    points.forEach(async point => {
      if (point.properties.description.includes(`!img${this.imageIDToFocus}`)) {
        this.imageToFocusPopup?.remove();
        this.imageToFocusPopup = new mapboxgl.Popup()
          .setLngLat(point.geometry.coordinates)
          .setHTML(await this.mapService.generateDescription(point.properties.description, point.properties?.name))
          .addTo(this.map);
      }

      if (point.properties.range) {
        const c = circle([point.geometry.coordinates[0], point.geometry.coordinates[1]], point.properties.rangeSize / 1000,
          {units: 'kilometers', steps: 100, properties: {id: point.properties.description}});
        this.circles.push(c);
      }
    });

    setTimeout(() => {
      this.map.addSource(circlesSourceID, {
        type: 'geojson',
        data: {
          type: 'FeatureCollection',
          features: this.circles
        }
      });
      this.sources.add(circlesSourceID);
      this.map.addLayer({
        id: circleLayerID,
        type: 'fill',
        source: circlesSourceID,
        paint: {
          'fill-opacity': 0.3,
          'fill-color': 'rgba(0,33,74,0.75)',
        }
      });
      this.layers.add(circleLayerID);
    }, 1000);

    this.map.on('click', pointsLayerID, async ev => {
      if (this.popupSem) {
        return;
      }
      this.popupSem = true;
      const position = ev.lngLat;
      let description = ev.features[0].properties.description;
      if (description.includes('!img')) {
        try {
          description = await this.mapService.generateDescription(description, ev.features[0].properties?.name);
        } catch (e) {
          description = '<p>You cannot view this image.</p>';
        }
      }
      new mapboxgl.Popup()
        .setLngLat(position)
        .setHTML(description)
        .on('close', () => this.popupSem = false)
        .addTo(this.map);
    });
  }

  private plotTrace(traces: any): void {
    const traceSourceID = 'trace';
    const traceLayerID = 'trace-layer';
    this.map.addSource(traceSourceID, {
      type: 'geojson',
      data: {
        type: 'FeatureCollection',
        features: traces
      }
    });
    this.sources.add(traceSourceID);
    this.map.addLayer({
      id: traceLayerID,
      type: 'line',
      source: traceSourceID,
      layout: {
        'line-join': 'round',
        'line-cap': 'round'
      },
      paint: {
        'line-color': '#0d38c5',
        'line-width': 3
      }
    });
    this.layers.add(traceLayerID);

    this.map.on('click', traceLayerID, ev => {
      if (this.popupSem) {
        return;
      }
      this.popupSem = true;
      const position = ev.lngLat;
      new mapboxgl.Popup()
        .setLngLat(position)
        .on('close', () => this.popupSem = false)
        .setHTML(`<h6>${ev.features[0].properties?.name}</h6>
            <p>${ev.features[0].properties.description}</p>`)
        .addTo(this.map);
    });
    this.addHoverEvents(traceLayerID);
  }

  addHoverEvents(sourceID: string): void {
    this.map.on('mousemove', sourceID, () => {
      this.map.getCanvas().style.cursor = 'pointer';
    });
    this.map.on('mouseleave', sourceID, () => {
      this.map.getCanvas().style.cursor = '';
    });
  }

}
