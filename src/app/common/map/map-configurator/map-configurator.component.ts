import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {UntypedFormBuilder, UntypedFormGroup, Validators} from '@angular/forms';
import {MapService} from '../../../services/map.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {CollectionSummary, MapSummary} from '../../models/rafalis-model';
import {CollectionService} from '../../../services/collection.service';

@Component({
  selector: 'app-map-configurator',
  templateUrl: './map-configurator.component.html',
  styleUrls: ['./map-configurator.component.css']
})
export class MapConfiguratorComponent implements OnInit, OnChanges {

  @Input() editMode = false;
  @Input() baseMap: MapSummary;
  @Input() readOnly = false;
  @Output() deleted = new EventEmitter<number>();

  private mapID: number;
  addMapForm: UntypedFormGroup;
  file: File;
  uploading = false;
  collections: CollectionSummary[];

  constructor(private builder: UntypedFormBuilder,
              private readonly mapService: MapService,
              private snackbar: MatSnackBar,
              private collectionService: CollectionService) {
    this.addMapForm = this.builder.group({
      name: ['', Validators.required],
      associatedCollection: ['-1', Validators.required]
    });
  }

  async ngOnInit(): Promise<void> {
    this.collections = await this.collectionService.getCollectionSummary();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.mapID = changes.baseMap.currentValue.id;
    this.addMapForm.patchValue({name: this.baseMap.name});
    this.addMapForm.patchValue({
      associatedCollection: this.baseMap.referenced_collection === null ?
        -1 : this.baseMap.referenced_collection
    });
    this.file = null;
  }

  closeDialog(): void {
    this.mapService.closed.emit();
  }

  onFileSelected(event): void {
    this.file = event.target.files[0];
  }

  async submitNewMap(): Promise<void> {
    if (!this.addMapForm.valid || !this.ensureMapNameIsOk()) {
      return;
    }
    this.uploading = true;
    const form = new FormData();
    if (!this.file) {
      const defaultJson = new File([this.mapService.generateEmptyGeoJson(this.addMapForm.value.name)], this.addMapForm.value.name, {
        type: 'application/json',
      });
      form.append('json', defaultJson);
      form.append('filename', this.addMapForm.value.name);
    } else {
      form.append('json', this.file);
      form.append('filename', this.file.name);
    }
    form.append('name', this.addMapForm.value.name);
    form.append('associatedCollection', `${this.addMapForm.value.associatedCollection}`);
    this.mapService.insertMap(form).then(confirm => {
      if (confirm.message === 'success') {
        this.snackbar.open('Map successfully added !', '', {duration: 5000});
        this.closeDialog();
      } else {
        this.snackbar.open(confirm.message, 'I got it', {verticalPosition: 'top'});
      }
    })
      .catch(e => {
        this.snackbar.open('Unable to create the new map, please try again.' + (e?.message ? '(' + e.message + ')' : ''),
          '', {verticalPosition: 'top', duration: 5000});
      })
      .finally(() => {
        this.uploading = false;
      });
    this.uploading = false;
  }

  ensureMapNameIsOk(): boolean {
    if (this.addMapForm.value.name === MapService.RESERVED_NAME || this.file?.name === MapService.RESERVED_FILENAME) {
      this.snackbar.open('Sorry, \'' + MapService.RESERVED_NAME + '\' and \'' + MapService.RESERVED_FILENAME + '\' are reserved names.',
        '', {verticalPosition: 'top', duration: 5000});
      return false;
    }
    return true;
  }

  async modifyMap(): Promise<void> {
    if (!this.editMode) {
      return;
    }
    this.uploading = true;
    const form = new FormData();
    if (!!this.file) {
      form.append('json', this.file);
      form.append('filename', this.file.name);
    } else {
      form.append('filename', this.baseMap.filename);
    }
    form.append('name', this.addMapForm.value.name);
    form.append('associatedCollection', `${this.addMapForm.value.associatedCollection}`);
    try {
      this.mapService.updateMap(form, this.mapID).then(confirm => {
        if (confirm.message === 'success') {
          this.snackbar.open('Map successfully modified !', '', {duration: 5000});
        } else {
          this.snackbar.open(confirm.message, 'I got it', {verticalPosition: 'top'});
        }
      });
    } catch (e) {
      this.snackbar.open(e.message, 'I got it', {verticalPosition: 'top'});
    }
    this.uploading = false;
  }

  async deleteMap(): Promise<void> {
    this.deleted.emit(this.mapID);
  }
}
