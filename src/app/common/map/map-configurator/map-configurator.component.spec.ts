import {ComponentFixture, TestBed} from '@angular/core/testing';

import {MapConfiguratorComponent} from './map-configurator.component';

describe('AddMapComponent', () => {
  let component: MapConfiguratorComponent;
  let fixture: ComponentFixture<MapConfiguratorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MapConfiguratorComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MapConfiguratorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
