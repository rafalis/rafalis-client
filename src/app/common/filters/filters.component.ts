import {Component, ElementRef, HostListener, OnInit, ViewChild} from '@angular/core';
import {UntypedFormBuilder} from '@angular/forms';
import {Filters, FiltersService} from '../../services/filters.service';
import {ImageService} from '../../services/image.service';
import {Subject} from 'rxjs';
import {debounceTime} from 'rxjs/operators';
import {AuthService} from '../../services/auth.service';
import {MobileService} from '../../services/mobile.service';

export enum SORT_BY {
  RECENT_ASC = 'RECENT_ASC',
  RECENT_DESC = 'RECENT_DESC',
  TITLE_ASC = 'TITLE_ASC',
  TITLE_DESC = 'TITLE_DESC'
}

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss'],
})
export class FiltersComponent implements OnInit {

  @ViewChild('mainSearch') mainSearch: ElementRef;
  filters: Filters = {keywords: '', publicOnly: false, color: ''};
  searchUpdatedEmitter: Subject<Filters>;
  sortby: { recent: boolean, title: boolean, ascending: boolean };
  availableTags: string[] = [];
  clickedTag = '';
  colorFilters = [
    '#000000',
    '#426a92',
    '#5e7e97',
    '#56722d',
    '#abab3c',
    '#c76844',
    '#783a21',
    '#bc8fa8',
    '#d8d2c9',
    '#ffffff',
  ];
  panelOpenState = false;
  mobile = false;

  constructor(private formBuilder: UntypedFormBuilder,
              readonly filtersService: FiltersService,
              private imageService: ImageService,
              readonly auth: AuthService,
              private readonly mobileService: MobileService) {
    this.mobile = this.mobileService.isMobileScreen;
  }

  @HostListener('document:keypress', ['$event'])
  onKeyPress(event: KeyboardEvent): void {
    if (event.key === '/') {
      this.panelOpenState = !this.panelOpenState;
      event.preventDefault();
      if (this.panelOpenState) {
        scrollTo({top: 1000});
      }
    }
  }

  focusSearch(): void {
    if (!this.mobile) {
      this.mainSearch?.nativeElement.focus();
    }
  }

  ngOnInit(): void {
    this.filters = this.filtersService.filters;
    this.sortby = this.filtersService.sortbyConfig;
    this.searchUpdatedEmitter = new Subject<Filters>();
    this.searchUpdatedEmitter.pipe(debounceTime(300))
      .subscribe(async (newSearch: Filters) => {
        this.filtersService.filters = newSearch;
        await this.imageService.updateImageItems();
        if (newSearch.keywords === '') {
          this.clickedTag = '';
        }
      });
    this.searchUpdatedEmitter.next(this.filters);
    if (this.filtersService.sortbyStr !== null) {
      this.filtersService.onSortByChange(this.filtersService.sortbyStr);
    }
    this.filtersService.newTagsAvailableEmitter.subscribe(newTags => {
      if (!newTags.includes(this.clickedTag)) {
        this.clickedTag = '';
      }
      this.availableTags = newTags;
    });
  }

  private getCurrentSortType(value): SORT_BY {
    let sortby: SORT_BY;
    switch (value) {
      case 'title':
        this.sortby.recent = false;
        this.sortby.title = true;
        sortby = this.sortby.ascending ? SORT_BY.TITLE_ASC : SORT_BY.TITLE_DESC;
        break;
      case 'recent':
        this.sortby.title = false;
        this.sortby.recent = true;
        sortby = this.sortby.ascending ? SORT_BY.RECENT_ASC : SORT_BY.RECENT_DESC;
        break;
      case 'ascending':
        this.sortby.ascending = true;
        sortby = this.sortby.recent ? SORT_BY.RECENT_ASC : SORT_BY.TITLE_ASC;
        break;
      case 'descending':
        this.sortby.ascending = false;
        sortby = this.sortby.recent ? SORT_BY.RECENT_DESC : SORT_BY.TITLE_DESC;
        break;
    }
    return sortby;
  }

  onRecentClick(): void {
    let value = 'recent';
    if (this.sortby.recent) {
      if (!this.sortby.ascending) {
        value = 'ascending';
      } else {
        this.sortby.recent = false;
        this.sortby.ascending = false;
        value = null;
      }
    }
    const sortby = this.getCurrentSortType(value);
    this.filtersService.onSortByChange(sortby);
  }

  onTitleClick(): void {
    let value = 'title';
    if (this.sortby.title) {
      if (!this.sortby.ascending) {
        value = 'ascending';
      } else {
        this.sortby.title = false;
        this.sortby.ascending = false;
        value = null;
      }
    }
    const sortby = this.getCurrentSortType(value);
    this.filtersService.onSortByChange(sortby);
  }

  async resetFilters(ev: MouseEvent): Promise<void> {
    ev.stopPropagation();
    this.sortby = this.filtersService.DEFAULT_SORTCONFIG;
    this.filtersService.sortbyStr = this.filtersService.DEFAULT_SORTSTR;
    this.filtersService.sortbyConfig = this.filtersService.DEFAULT_SORTCONFIG;
    this.filters.publicOnly = false;
    this.filtersService.publicToggleEmitter.emit(this.filters.publicOnly);
    this.filters.keywords = '';
    this.filters.color = '';
    this.clickedTag = '';
    await this.imageService.updateImageItems();
  }

  onTagClick(tag: string): void {
    if (this.clickedTag === '' || tag !== this.clickedTag) {
      this.filters.keywords = tag;
      this.clickedTag = tag;
    } else {
      this.clickedTag = '';
      this.filters.keywords = '';
    }
    this.searchUpdatedEmitter.next(this.filters);
  }

  async onColorFilterClick(color: string): Promise<void> {
    this.filters.color = this.filters.color === color ? '' : color;
    this.searchUpdatedEmitter.next(this.filters);
  }

  getCurrentActiveFiltersStatus(): string {
    let active = 0;
    if (this.filters.color !== '') {
      active += 1;
    }
    if (this.filters.keywords !== '') {
      active += 1;
    }
    if (this.filters.publicOnly) {
      active += 1;
    }
    if (active > 0) {
      if (this.filters.publicOnly) {
        return this.imageService.currentItems.filter(item => item.ispublic).length + ' items, ' + active + ' active filter(s)';
      } else {
        return this.imageService.currentItems.length + ' items, ' + active + ' active filter(s)';
      }
    } else {
      return 'Filter/sort by...';
    }
  }
}
