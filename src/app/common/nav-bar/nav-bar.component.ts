import {Component, HostListener, Input, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {MobileService} from '../../services/mobile.service';
import {ActivatedRoute, Router, UrlSegment} from '@angular/router';
import {ScrollService} from '../../services/scroll.service';
import {Title} from '@angular/platform-browser';

interface MenuItem {
  name: string;
  key: string;
  routerLink: string;
  icon: string;
}

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {

  @Input()
  activatedItem = '';
  menuItems: MenuItem[] = [];
  isOnCreatorPage = false;

  constructor(readonly authService: AuthService,
              private mobileService: MobileService,
              readonly router: Router,
              private scrollService: ScrollService,
              private titleService: Title,
              private activatedRoute: ActivatedRoute) {
  }

  @HostListener('window:popstate', ['$event'])
  onPopState(): void {
    if (location.pathname === '/') {
      this.scrollService.enableScrollBack();
    }
  }

  ngOnInit(): void {
    this.menuItems = [];
    this.menuItems.push({key: 'home', name: 'Home', routerLink: '/', icon: 'home'});
    this.menuItems.push({key: 'collections', name: 'Collections', routerLink: '/collections', icon: 'photo_library'});
    this.menuItems.push({key: 'map', name: 'Maps', routerLink: '/maps', icon: 'map'});

    this.activatedRoute.url.subscribe((newURL) => this.onURLChange(newURL));
  }

  goToLoginPage(): void {
    this.router.navigate(['login'], this.router.url === '/' ? {} : {queryParams: {returnUrl: this.router.url}});
  }

  private onURLChange(newURL: UrlSegment[]): void {
    if (newURL.length === 0) {
      this.titleService.setTitle('Rafalis');
    }
    this.isOnCreatorPage = location.href.includes('admin');
  }
}
