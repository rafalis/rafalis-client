import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {ImageService} from '../../services/image.service';
import {SafeUrl} from '@angular/platform-browser';
import {IMAGE_QUALITY} from '../models/rafalis-model';


export interface ImageClickedEvent {
  id: number;
  url: SafeUrl;
}

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.scss']
})
export class ImageComponent implements OnInit, OnChanges {

  @Input() imageID: number;
  @Input() alt = '<image unavailable>';
  @Input() thumbnail = false;
  @Input() title = '';
  @Input() classes = '';
  @Output() imageclicked = new EventEmitter<ImageClickedEvent>();

  imageURL: SafeUrl;
  showImage = false;

  constructor(private auth: AuthService,
              private imageService: ImageService) {
  }

  async ngOnInit(): Promise<void> {
    await this.getImage();
  }

  async ngOnChanges(changes: SimpleChanges): Promise<void> {
    if (changes.imageID?.isFirstChange() || (!changes?.imageID && !changes?.thumbnail)) {
      return;
    }
    this.imageURL = null;
    if (!this.classes.includes('image-hide')) {
      this.classes = this.classes.replace('image-shown', '');
      this.classes += ' image-hide';
    }
    if (changes.imageID?.previousValue !== changes.imageID?.currentValue && !isNaN(changes.imageID?.currentValue)) {
      await this.getImage();
    }
    if (changes.thumbnail?.previousValue !== changes.thumbnail?.currentValue) {
      await this.getImage();
    }
  }

  private async getImage(): Promise<void> {
    if (!this.imageID) {
      this.showImage = false;
      return;
    }
    try {
      if (this.auth.authenticated) {
        const blobResponse = await this.imageService.getImageBytes(this.imageID,
          this.thumbnail ? IMAGE_QUALITY.THUMB_QUALITY : IMAGE_QUALITY.SMALL_QUALITY);
        this.imageURL = this.imageService.createImageFromBlob(blobResponse.body);
      } else {
        this.imageURL = this.imageService.getImageURL(this.imageID,
          this.thumbnail ? IMAGE_QUALITY.THUMB_QUALITY : IMAGE_QUALITY.SMALL_QUALITY);
      }
      this.showImage = true;
      this.classes = this.classes.replace('image-hide', 'image-shown');
    } catch (e) {
      console.log('Image #' + this.imageID + ' not found ');
      this.showImage = false;
    }
  }

}
